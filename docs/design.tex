% Created 2024-03-24 Sun 13:15
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[a4paper, lmargin=25mm, rmargin=25mm, tmargin=25mm, bmargin=25mm]{geometry}
\usepackage{xurl}
\date{2024 March 23}
\title{7Units Design Document\\\medskip
\large For version 0.5.0}
\hypersetup{
 pdfauthor={},
 pdftitle={7Units Design Document},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.2 (Org mode 9.6.15)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents

\newpage

\section{Introduction}
\label{sec:orgdac8c13}
7Units is a program that can convert between units.  This document details the internal design of 7Units, intended to be used by current and future developers.
\section{System Overview}
\label{sec:org2a8e77a}
\begin{figure}[htbp]
\centering
\includegraphics[height=144px]{./diagrams/overview-diagram.plantuml.png}
\caption{A big-picture diagram of 7Units, containing all of the major classes.}
\end{figure}
\subsection{Packages of 7Units}
\label{sec:org80ac84e}
7Units splits its code into three main packages:
\begin{description}
\item[{\texttt{sevenUnits.unit}}] The \hyperref[sec:orgc2400d6]{unit system}
\item[{\texttt{sevenUnits.utils}}] Extra classes that aid the unit system.
\item[{\texttt{sevenUnitsGUI}}] The \hyperref[sec:org261b06e]{front end} code
\end{description}
\texttt{sevenUnits.unit} depends on \texttt{sevenUnits.utils}, while \texttt{sevenUnitsGUI} depends on both \texttt{sevenUnits} packages.  There is only one class that isn't in any of these packages, \texttt{sevenUnits.VersionInfo}.
\subsection{Major Classes of 7Units}
\label{sec:org5910307}
\begin{description}
\item[{\hyperref[sec:org946a4e5]{sevenUnits.unit.Unit}}] The class representing a unit
\item[{\hyperref[sec:orgac71770]{sevenUnits.unit.UnitDatabase}}] A class that stores collections of units, prefixes and dimensions.
\item[{\hyperref[sec:org57b8a42]{sevenUnitsGUI.View}}] The class that handles interaction between the user and the program.
\item[{\hyperref[sec:orga668171]{sevenUnitsGUI.Presenter}}] The class that handles communication between the \texttt{View} and the unit system.
\end{description}
\newpage
\subsection{Process of Unit Conversion}
\label{sec:orgbbad9d5}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./diagrams/convert-units.plantuml.png}
\caption{The process of converting units}
\end{figure}
\begin{enumerate}
\item The \texttt{View} triggers a unit conversion method from the \texttt{Presenter}.
\item The \texttt{Presenter} gets raw input data from the \texttt{View}'s public methods (from unit, to unit, from value).
\item The \texttt{Presenter} uses the \texttt{UnitDatabase}'s methods to convert the raw data from the \texttt{View} into actual units.
\item The \texttt{Presenter} performs the unit conversion using the provided unit objects.
\item The \texttt{Presenter} calls the \texttt{View}'s methods to show the result of the conversion.
\end{enumerate}
\newpage
\subsection{Process of Expression Conversion}
\label{sec:org52749d5}
The process of expression conversion is similar to that of unit conversion.
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./diagrams/convert-expressions.plantuml.png}
\caption{The process of converting expressions}
\end{figure}
\begin{enumerate}
\item The \texttt{View} triggers a unit conversion method from the \texttt{Presenter}.
\item The \texttt{Presenter} gets raw input data from the \texttt{View}'s public methods (unit conversion: from unit, to unit, from value; expression conversion: input expression, output expression).
\item The \texttt{Presenter} uses the \texttt{UnitDatabase}'s methods to parse the expressions, converting the input into a \texttt{LinearUnitValue} and the output into a \texttt{Unit}.
\item The \texttt{Presenter} converts the provided value into the provided unit.
\item The \texttt{Presenter} calls the \texttt{View}'s methods to show the result of the conversion.
\end{enumerate}
\newpage
\section{Unit System Design}
\label{sec:orgc2400d6}
Any code related to the backend unit system is stored in the \texttt{sevenUnits.unit} package.

Here is a class diagram of the system.  Unimportant methods, methods inherited from Object, getters and setters have been omitted.
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./diagrams/units-class-diagram.plantuml.png}
\caption{Class diagram of sevenUnits.unit}
\end{figure}
\newpage
\subsection{Dimensions}
\label{sec:orgb21aaed}
Dimensions represent what a unit is measuring, such as length, time, or energy.  Dimensions are represented as an \hyperref[sec:org16bc96f]{ObjectProduct}<BaseDimension>, where \texttt{BaseDimension} is a very simple class (its only properties are a name and a symbol) which represents the dimension of a base unit; these base dimensions can be multiplied to create all other Dimensions.
\subsection{Unit Classes}
\label{sec:org946a4e5}
Units are internally represented by the abstract class \texttt{Unit}.  All units have an \hyperref[sec:org16bc96f]{ObjectProduct}<BaseUnit> (referred to as the base) that they are based on, a dimension (ObjectProduct<BaseDimension>), one or more names and a symbol (these last two bits of data are contained in the \texttt{NameSymbol} class).  The dimension is calculated from the base unit when needed; the variable is just a cache.  It has two constructors: a package-private one used to make \texttt{BaseUnit} instances, and a protected one used to make general units (for other subclasses of \texttt{Unit}).  All unit classes are immutable.

Units also have two conversion functions - one which converts from a value expressed in this unit to its base unit, and another which converts from a value expressed in the base unit to this unit.  In \texttt{Unit}, they are defined as two abstract methods.  This allows you to convert from any unit to any other (as long as they have the same base, i.e. you aren't converting metres to pounds).  To convert from A to B, first convert from A to its base, then convert from the base to B.

\texttt{BaseUnit} represents a unit that all other units are defined by.  All of the units used by this system are defined by seven SI \texttt{BaseUnit} instances (metre, second, kilogram, ampere, kelvin, mole, candela; this is what 7Units is named after) and two non-SI \texttt{BaseUnit} instances (US dollar and bit).  Because base units are themselves units (and should be able to be used as units), \texttt{BaseUnit} is a subclass of \texttt{Unit}, using its own package-private constructor.

However, most units are instances of \texttt{LinearUnit}, another subclass of \texttt{Unit}.  \texttt{LinearUnit} represents a unit that is \emph{a product of a base unit and a constant called the \textbf{conversion factor}}.  Most units you've ever used fall under this definition, the only common exceptions are degrees Celsius and Fahrenheit.  This simplicity allows the \texttt{LinearUnit} to do many things:
\begin{itemize}
\item It can implement conversion to and from the base as multiplying and dividing by the conversion factor respectively
\item You can easily create new units by multiplying or dividing a \texttt{LinearUnit} by a number (for example, kilometre = metre * 1000).  This can be easily implemented as multiplying this unit's conversion factor by the multiplier and returning a new \texttt{LinearUnit} with that conversion factor factor.
\item You can add or subtract two \texttt{LinearUnit} instances to create a third (as long as they have the same base) by adding or subtracting the conversion factor.
\item You can multiply or divide any two \texttt{LinearUnit} instances to create a third by multiplying or dividing the bases and conversion factors.
\item Note that any operations will return a unit without name(s) or a symbol.  All unit classes have a \texttt{withName} method that returns a copy of them with different names and/or a different symbol (all of this info is contained in the \texttt{NameSymbol} class)
\end{itemize}

There are a few more classes which play small roles in the unit system:
\begin{description}
\item[{Unitlike}] A class that is like a unit, but its "value" can be any class.  The only use of this class right now is to implement \texttt{MultiUnit}, a combination of units (like "foot + inch", commonly used in North America for measuring height); its "value" is a list of numbers.
\item[{FunctionalUnit}] A convenience class that implements the two conversion functions of \texttt{Unit} using \texttt{DoubleUnaryOperator} instances.  This is used internally to implement degrees Celsius and Fahrenheit.  There is also a version of this for \texttt{Unitlike}, \texttt{FunctionalUnitlike}.
\item[{UnitValue}] A value expressed as a certain unit (such as "7 inches").  This class is used by the simple unit converter to represent units.  You can convert them between units.  There are also versions of this for \texttt{LinearUnit} and \texttt{Unitlike}.
\item[{Metric}] A static utility class with instances of all of the SI named units, the 9 base dimensions, SI prefixes, some common prefixed units like the kilometre, and a few non-SI units used commonly with them.
\item[{BritishImperial}] A static utility class with instances of common units in the British Imperial system (not to be confused with the US Customary system, which is also called "Imperial"; it has the same unit names but the values of a few units are different).  This class and the US Customary is divided into static classes for each dimension, such as \texttt{BritishImperial.Length}.
\item[{USCustomary}] A static utility class with instances of common units in the US Customary system (not to be confused with the British Imperial system; it has the same unit names but the values of a few units are different).
\end{description}
\subsection{Prefixes}
\label{sec:orgede1b85}
A \texttt{UnitPrefix} is a simple object that can multiply a \texttt{LinearUnit} by a value.  It can calculate a new name for the unit by combining its name and the unit's name (symbols are done similarly).  It can do multiplication, division and exponentation with a number, as well as multiplication and division with another prefix; all of these work by changing the prefix's multiplier.
\subsection{The Unit Database}
\label{sec:orgac71770}
The \texttt{UnitDatabase} class stores all of the unit, prefix and dimension data used by this program.  It is not a representation of an actual database, just a class that stores lots of data.

Units are stored using a custom \texttt{Map} implementation (\texttt{PrefixedUnitMap}) which maps unit names to units.  It is backed by two maps: one for units (without prefixes) and one for prefixes.  It is programmed to include prefixes (so if units includes "metre" and prefixes includes "kilo", this map will include "kilometre", mapping it to a unit representing a kilometre).  It is immutable, but you can modify the underlying maps, which is reflected in the \texttt{PrefixedUnitMap}.  Other than that, it is a normal map implementation.

Prefixes and dimensions are stored in normal maps.
\subsubsection{Parsing Expressions}
\label{sec:org02e3ff1}
Each \texttt{UnitDatabase} instance has four \hyperref[sec:org7296a14]{ExpressionParser} instances associated with it, for four types of expressions: unit, unit value, prefix and dimension.  They are mostly similar, with operators corresponding to each operation of the corresponding class (\texttt{LinearUnit}, \texttt{LinearUnitValue}, \texttt{UnitPrefix}, \texttt{ObjectProduct<BaseDimension>}).  Unit and unit value expressions use linear units; nonlinear units can be used with a special syntax (like "degC(20)") and are immediately converted to a linear unit representing their base (Kelvin in this case) before operating.
\subsubsection{Parsing Files}
\label{sec:org676148d}
There are two types of data files: unit and dimension.

Unit files contain data about units and prefixes.  Each line contains the name of a unit or prefix (prefixes end in a dash, units don't) followed by an expression which defines it, separated by one or more space characters (this behaviour is defined by the static regular expression \texttt{NAME\_EXPRESSION}).  Unit files are parsed line by line, each line being run through the \texttt{addUnitOrPrefixFromLine} method, which splits a line into name and expression, determines whether it's a unit or a prefix, and parses the expression.  Because all units are defined by others, base units need to be defined with a special expression "!"; \textbf{these units should be added to the database before parsing the file}.

Dimension files are similar, only for dimensions instead of units and prefixes.
\newpage
\section{Front-End Design}
\label{sec:org261b06e}
The front-end of 7Units is based on an MVP model.  There are two major frontend classes, the \textbf{View} and the \textbf{Presenter}.
\subsection{The View}
\label{sec:org57b8a42}
The \texttt{View} is the part of the frontend code that directly interacts with the user.  It handles input and output, but does not do any processing.  Processing is handled by the presenter and the backend code.

The \texttt{View} is an interface, not a single class, so that I can easily create multiple views without having to rewrite any processing code.  This allows me to easily prototype changes to the GUI without messing with existing code.

In addition, I have decided to move some functions of the \texttt{View} into two subinterfaces, \texttt{UnitConversionView} and \texttt{ExpressionConversionView}.  This is because 7Units supports two kinds of unit conversion: unit conversion (select two compatible units, specify a value then convert) and expression conversion (enter two expressions and convert the first to a multiple of the second).  Putting these functions into subinterfaces allows a \texttt{View} to do one type of conversion without forcing it to support the other.

There are currently two implementations of the \texttt{View}:
\begin{description}
\item[{TabbedView}] A Swing GUI implementation that uses tabs to separate the two types of conversion.  The default GUI used by 7Units.
\item[{ViewBot}] A simulated view that allows programs to set the output of its public methods (i.e. every getter in \texttt{View} has a setter in \texttt{ViewBot}).  Intended for testing, and is already used by \texttt{PresenterTest}.
\end{description}
Both of these \texttt{View} implementations implement \texttt{UnitConversionView} and \texttt{ExpressionConversionView}.
\subsection{The Presenter}
\label{sec:orga668171}
The \texttt{Presenter} is an intermediary between the \texttt{View} and the backend code.  It accepts the user's input and passes it to the backend, then accepts the backend's output and passes it to the frontend for user viewing.  Its main functions do not have arguments or return values; instead it takes input from and provides output to the \texttt{View} via its public methods.
\subsubsection{Rules}
\label{sec:org81f6f8a}
The \texttt{Presenter} has a set of function-object rules that determine some of its behaviours.  Each corresponds to a setting in the \texttt{View}, but they can be set to other values via the \texttt{Presenter}'s setters (although nonstandard rules cannot be saved and loaded):
\begin{description}
\item[{numberDisplayRule}] A function that determines how numbers are displayed.  This controls the rounding rules.
\item[{prefixRepetitionRule}] A function that determines which combinations of prefixes are valid in unit expressions.  This controls the unit prefix rules.
\item[{searchRule}] A function that determines which prefixes are shown in the unit lists in unit conversion (which prefixed units are searchable).
\end{description}

These rules have been made this way to enable an incredible level of customization of these behaviours.  Because any function object with the correct arguments and return type is accepted, these rules (especially the number display rule) can do much more than the default behaviours.
\subsection{Utility Classes}
\label{sec:orga5b57ce}
The frontend has many miscellaneous utility classes.  Many of them are package-private.  Here is a list of them, with a brief description of what they do and where they are used:
\begin{description}
\item[{DefaultPrefixRepetitionRule}] An enum containing the available rules determining when you can repeat prefixes.  Used by the \texttt{TabbedView} for selecting the rule and by the \texttt{Presenter} for loading it from a file.
\item[{DelegateListModel}] A \texttt{javax.swing.ListModel} implementation that delegates all of its methods to a \texttt{List}.  Implements \texttt{List} by also delegating to the underlying list.  Used by the \texttt{SearchBoxList} to create an easily mutable \texttt{ListModel}.
\item[{FilterComparator}] A \texttt{Comparator} that sorts objects according to whether or not they match a filter.  Used by the \texttt{SearchBoxList} for item sorting.
\item[{GridBagBuilder}] A convenience class for generating \texttt{GridBagConstraints} objects for Swing's \texttt{GridBagLayout}.  Used by \texttt{TabbedView} for constructing the GUI.
\item[{SearchBoxList}] A Swing component that combines a text field and a \texttt{JList} to create a searchable list.  Used by the \texttt{TabbedView}'s unit conversion mode.
\item[{StandardDisplayRules}] A static utility class that allows you to generate display/rounding rules.  Used by \texttt{TabbedView} for generating these rules and \texttt{Presenter} for loading them from a file.
\item[{UnitConversionRecord}] A record-like object that contains the results of a unit or expression conversion.  Used by \texttt{UnitConversionView} and \texttt{ExpressionConversionView} for accepting the results to be displayed.
\end{description}
\newpage
\section{Utility Classes}
\label{sec:org3686e64}
7Units has a few general "utility" classes.  They aren't directly related to units, but are used in the units system.
\subsection{ObjectProduct}
\label{sec:org16bc96f}
An \texttt{ObjectProduct} represents a "product" of elements of some type.  The units system uses them to represent coherent units as a product of base units, and dimensions as a product of base dimensions.

Internally, it is represented using a map mapping objects to their exponents in the product.  For example, the unit "kg m\textsuperscript{2} / s\textsuperscript{2}" (i.e. a Joule) would be represented with a map like \texttt{[kg: 1, m: 2, s: -2]}.
\subsection{ExpressionParser}
\label{sec:org7296a14}
The \texttt{ExpressionParser} class is used to parse the unit, prefix and dimension expressions that are used throughout 7Units.  An expression is something like "(2 m + 30 J / N) * 8 s)".  Each instance represents a type of expression, containing a way to obtain values (such as numbers or units) from the text and operations that can be done on these values (such as addition, subtraction or multiplication).  Each operation also has a priority, which controls the order of operations (i.e. multiplication gets a higher priority than addition).

\texttt{ExpressionParser} has a parameterized type \texttt{T}, which represents the type of the value used in the expression.  The expression parser currently only supports one type of value per expression; in the expressions used by 7Units numbers are treated as a kind of unit or prefix.  Operators are represented by internal types; the system distinguishes between unary operators (those that take a single value, like negation) and binary operators (those that take 2 values, like +, -, * or /).

There is one exception to this rule - users are allowed to create "numeric operators" that take one argument of type \texttt{T} and one numeric argument.  This is intended for exponentation, but could also be used for vector scaling.

Expressions are parsed in 2 steps:
\begin{enumerate}
\item Convert the expression to \href{https://en.wikipedia.org/wiki/Reverse\_Polish\_notation}{Reverse Polish Notation}, where operators come \textbf{after} the values they operate on, and brackets and the order of operations are not necessary.  For example, "2 + 5" becomes "\texttt{2 5 +}", "(1 + 2) * 3" becomes "\texttt{1 2 + 3 *}" and the example expression earlier becomes "\texttt{2 m * 30 J * N / + 8 s * *}".  This makes it simple to evaluate - early calculators used RPN for a good reason!
\item Evaluate the RPN expression.  This can be done simply with a for loop and a stack.  For each token in the expression, the progam does the following:
\begin{itemize}
\item if it is a number or unit, add it to the stack.
(the dimension parser adds numbers to a separate stack for type safety, as numbers cannot be stored as a dimension type like they can with units.)
\item if it is a unary operator, take one value from the stack, apply the operator to it, and put the result into the stack.
\item if it is a binary operator, take two values from the stack, apply the operator to them, and put the result into the stack.
\item if it is a numeric operator, take one value from the stack and one number from the numeric stack, apply to the operator to the two, and put the result in the regular stack.
\end{itemize}
After evaluating the last token, there should be one value left in the stack - the answer.  If there isn't, the original expression was malformed.
\end{enumerate}
\subsection{Math Classes}
\label{sec:orgfd8c723}
There are two simple math classes in 7Units:
\begin{description}
\item[{\texttt{UncertainDouble}}] Like a \texttt{double}, but with an uncertainty (e.g. \(2.0 \pm 0.4\)).  The operations are like those of the regular Double, only they also calculate the uncertainty of the final value.  They also have "exact" versions to help interoperation between \texttt{double} and \texttt{UncertainDouble}.  It is used by the converter's Scientific Precision setting.
\item[{\texttt{DecimalComparison}}] A static utility class that contains a few alternate equals() methods for \texttt{double} and \texttt{UncertainDouble}.  These methods allow a slight (configurable) difference between values to still be considered equal, to fight roundoff error.
\end{description}
\subsection{Collection Classes}
\label{sec:org32d7d09}
The \texttt{ConditionalExistenceCollections} class contains wrapper implementations of \texttt{Collection}, \texttt{Iterator}, \texttt{Map} and \texttt{Set}.  These implementations ignore elements that do not pass a certain condition - if an element fails the condition, \texttt{contains} will return false, the iterator will skip past it, it won't be counted in \texttt{size}, etc. even if it exists in the original collection.  Effectively, any element of the original collection that fails the test does not exist.
\end{document}
