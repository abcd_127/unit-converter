/**
 * Copyright (C) 2020 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnits.unit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.Test;

/**
 * Tests related to the {@code MultiUnit}.
 *
 * @since 2020-10-03
 */
class MultiUnitTest {

	/**
	 * Ensures that the {@code MultiUnit} can convert properly.
	 */
	@Test
	final void testConvert() {
		final Random rng = ThreadLocalRandom.current();
		final MultiUnit footInch = MultiUnit.of(BritishImperial.Length.FOOT,
				BritishImperial.Length.INCH);

		assertEquals(1702.0,
				footInch.convertTo(Metric.METRE.withPrefix(Metric.MILLI),
						Arrays.asList(5.0, 7.0)),
				1.0);

		for (int i = 0; i < 1000; i++) {
			final double feet = rng.nextInt(1000);
			final double inches = rng.nextDouble() * 12;
			final double millimetres = feet * 304.8 + inches * 25.4;

			final List<Double> feetAndInches = Metric.METRE
					.withPrefix(Metric.MILLI).convertTo(footInch, millimetres);
			assertEquals(feet, feetAndInches.get(0), 1e-10);
			assertEquals(inches, feetAndInches.get(1), 1e-10);
		}
	}

	/**
	 * Test method for {@link sevenUnits.unit.MultiUnit#convertFromBase(double)}.
	 */
	@Test
	final void testConvertFromBase() {
		final Random rng = ThreadLocalRandom.current();
		final MultiUnit footInch = MultiUnit.of(BritishImperial.Length.FOOT,
				BritishImperial.Length.INCH);

		// 1.7 m =~ 5' + 7"
		final List<Double> values = footInch.convertFromBase(1.7018);

		assertEquals(5, values.get(0));
		assertEquals(7, values.get(1), 1e-12);

		for (int i = 0; i < 1000; i++) {
			final double feet = rng.nextInt(1000);
			final double inches = rng.nextDouble() * 12;
			final double metres = feet * 0.3048 + inches * 0.0254;

			final List<Double> feetAndInches = footInch.convertFromBase(metres);
			assertEquals(feet, feetAndInches.get(0), 1e-10);
			assertEquals(inches, feetAndInches.get(1), 1e-10);
		}
	}

	/**
	 * Test method for
	 * {@link sevenUnits.unit.MultiUnit#convertToBase(java.util.List)}.
	 */
	@Test
	final void testConvertToBase() {
		final Random rng = ThreadLocalRandom.current();
		final MultiUnit footInch = MultiUnit.of(BritishImperial.Length.FOOT,
				BritishImperial.Length.INCH);

		// 1.7 m =~ 5' + 7"
		assertEquals(1.7018, footInch.convertToBase(Arrays.asList(5.0, 7.0)),
				1e-12);

		for (int i = 0; i < 1000; i++) {
			final double feet = rng.nextInt(1000);
			final double inches = rng.nextDouble() * 12;
			final double metres = feet * 0.3048 + inches * 0.0254;

			assertEquals(metres,
					footInch.convertToBase(Arrays.asList(feet, inches)), 1e-12);
		}
	}
}
