/**
 * Copyright (C) 2022 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnitsGUI;

import java.math.RoundingMode;

import sevenUnits.unit.LinearUnitValue;
import sevenUnits.unit.UnitValue;

/**
 * A record of a conversion between units or expressions
 *
 * @since v0.4.0
 * @since 2022-04-09
 */
public final class UnitConversionRecord {
	/**
	 * Gets a {@code UnitConversionRecord} from two linear unit values
	 *
	 * @param input  input unit & value
	 * @param output output unit & value
	 * @return unit conversion record
	 * @since v0.4.0
	 * @since 2022-04-09
	 */
	public static UnitConversionRecord fromLinearValues(LinearUnitValue input,
			LinearUnitValue output) {
		return UnitConversionRecord.valueOf(input.getUnit().getName(),
				output.getUnit().getName(),
				input.getValue().toString(false, RoundingMode.HALF_EVEN),
				output.getValue().toString(false, RoundingMode.HALF_EVEN));
	}

	/**
	 * Gets a {@code UnitConversionRecord} from two unit values
	 *
	 * @param input  input unit & value
	 * @param output output unit & value
	 * @return unit conversion record
	 * @since v0.4.0
	 * @since 2022-04-09
	 */
	public static UnitConversionRecord fromValues(UnitValue input,
			UnitValue output) {
		return UnitConversionRecord.valueOf(input.getUnit().getName(),
				output.getUnit().getName(), String.valueOf(input.getValue()),
				String.valueOf(output.getValue()));
	}

	/**
	 * Gets a {@code UnitConversionRecord}
	 *
	 * @param fromName          name of unit or expression that was converted
	 *                          from
	 * @param toName            name of unit or expression that was converted to
	 * @param inputValueString  string representing input value
	 * @param outputValueString string representing output value
	 * @return unit conversion record
	 * @since v0.4.0
	 * @since 2022-04-09
	 */
	public static UnitConversionRecord valueOf(String fromName, String toName,
			String inputValueString, String outputValueString) {
		return new UnitConversionRecord(fromName, toName, inputValueString,
				outputValueString);
	}

	/**
	 * The name of the unit or expression that was converted from
	 */
	private final String fromName;
	/**
	 * The name of the unit or expression that was converted to
	 */
	private final String toName;

	/**
	 * A string representing the input value. It doesn't need to be the same as
	 * the input value's string representation; it could be rounded, for example.
	 */
	private final String inputValueString;
	/**
	 * A string representing the input value. It doesn't need to be the same as
	 * the input value's string representation; it could be rounded, for example.
	 */
	private final String outputValueString;

	/**
	 * @param fromName          name of unit or expression that was converted
	 *                          from
	 * @param toName            name of unit or expression that was converted to
	 * @param inputValueString  string representing input value
	 * @param outputValueString string representing output value
	 * @since 2022-04-09
	 */
	private UnitConversionRecord(String fromName, String toName,
			String inputValueString, String outputValueString) {
		this.fromName = fromName;
		this.toName = toName;
		this.inputValueString = inputValueString;
		this.outputValueString = outputValueString;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof UnitConversionRecord))
			return false;
		final UnitConversionRecord other = (UnitConversionRecord) obj;
		if (this.fromName == null) {
			if (other.fromName != null)
				return false;
		} else if (!this.fromName.equals(other.fromName))
			return false;
		if (this.inputValueString == null) {
			if (other.inputValueString != null)
				return false;
		} else if (!this.inputValueString.equals(other.inputValueString))
			return false;
		if (this.outputValueString == null) {
			if (other.outputValueString != null)
				return false;
		} else if (!this.outputValueString.equals(other.outputValueString))
			return false;
		if (this.toName == null) {
			if (other.toName != null)
				return false;
		} else if (!this.toName.equals(other.toName))
			return false;
		return true;
	}

	/**
	 * @return name of unit or expression that was converted from
	 * @since v0.4.0
	 * @since 2022-04-09
	 */
	public String fromName() {
		return this.fromName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ (this.fromName == null ? 0 : this.fromName.hashCode());
		result = prime * result + (this.inputValueString == null ? 0
				: this.inputValueString.hashCode());
		result = prime * result + (this.outputValueString == null ? 0
				: this.outputValueString.hashCode());
		result = prime * result
				+ (this.toName == null ? 0 : this.toName.hashCode());
		return result;
	}

	/**
	 * @return string representing input value
	 * @since v0.4.0
	 * @since 2022-04-09
	 */
	public String inputValueString() {
		return this.inputValueString;
	}

	/**
	 * @return string representing output value
	 * @since v0.4.0
	 * @since 2022-04-09
	 */
	public String outputValueString() {
		return this.outputValueString;
	}

	/**
	 * @return name of unit or expression that was converted to
	 * @since v0.4.0
	 * @since 2022-04-09
	 */
	public String toName() {
		return this.toName;
	}

	@Override
	public String toString() {
		final String inputString = this.inputValueString.isBlank() ? this.fromName
				: this.inputValueString + " " + this.fromName;
		final String outputString = this.outputValueString.isBlank() ? this.toName
				: this.outputValueString + " " + this.toName;
		return inputString + " = " + outputString;
	}
}
