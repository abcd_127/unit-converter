/**
 * Copyright (C) 2020 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnits.unit;

import java.util.Optional;

import sevenUnits.utils.NameSymbol;

/**
 *
 * @since 2020-09-07
 */
final class UnitlikeValue<T extends Unitlike<V>, V> {
	/**
	 * Gets a {@code UnitlikeValue<V>}.
	 *
	 * @since 2020-10-02
	 */
	public static <T extends Unitlike<V>, V> UnitlikeValue<T, V> of(T unitlike,
			V value) {
		return new UnitlikeValue<>(unitlike, value);
	}

	private final T unitlike;
	private final V value;

	/**
	 * @param unitlike
	 * @param value
	 * @since 2020-09-07
	 */
	private UnitlikeValue(T unitlike, V value) {
		this.unitlike = unitlike;
		this.value = value;
	}

	/**
	 * @return true if this value can be converted to {@code other}.
	 * @since 2020-10-01
	 */
	public final boolean canConvertTo(Unit other) {
		return this.unitlike.canConvertTo(other);
	}

	/**
	 * @return true if this value can be converted to {@code other}.
	 * @since 2020-10-01
	 */
	public final <W> boolean canConvertTo(Unitlike<W> other) {
		return this.unitlike.canConvertTo(other);
	}

	/**
	 * Returns a UnitlikeValue that represents the same value expressed in a
	 * different unitlike form.
	 * 
	 * @param other new unit to express value in
	 * @return value expressed in {@code other}
	 */
	public final <U extends Unitlike<W>, W> UnitlikeValue<U, W> convertTo(
			U other) {
		return UnitlikeValue.of(other,
				this.unitlike.convertTo(other, this.getValue()));
	}

	/**
	 * Returns a UnitValue that represents the same value expressed in a
	 * different unit
	 * 
	 * @param other new unit to express value in
	 * @return value expressed in {@code other}
	 */
	public final UnitValue convertTo(Unit other) {
		return UnitValue.of(other,
				this.unitlike.convertTo(other, this.getValue()));
	}

	/**
	 * Returns this unit value represented as a {@code LinearUnitValue} with this
	 * unit's base unit as the base.
	 *
	 * @param ns name and symbol for the base unit, use NameSymbol.EMPTY if not
	 *           needed.
	 * @since 2020-09-29
	 */
	public final LinearUnitValue convertToBase(NameSymbol ns) {
		final LinearUnit base = LinearUnit.getBase(this.unitlike).withName(ns);
		return this.convertToLinear(base);
	}

	/**
	 * @return a {@code LinearUnitValue} that is equivalent to this value. It
	 *         will have zero uncertainty.
	 * @since 2020-09-29
	 */
	public final LinearUnitValue convertToLinear(LinearUnit other) {
		return LinearUnitValue.getExact(other,
				this.getUnitlike().convertTo(other, this.getValue()));
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof UnitlikeValue))
			return false;
		final UnitlikeValue<?, ?> other = (UnitlikeValue<?, ?>) obj;
		if (this.getUnitlike() == null) {
			if (other.getUnitlike() != null)
				return false;
		} else if (!this.getUnitlike().equals(other.getUnitlike()))
			return false;
		if (this.getValue() == null) {
			if (other.getValue() != null)
				return false;
		} else if (!this.getValue().equals(other.getValue()))
			return false;
		return true;
	}

	/**
	 * @return the unitlike
	 * @since 2020-09-29
	 */
	public final Unitlike<V> getUnitlike() {
		return this.unitlike;
	}

	/**
	 * @return the value
	 * @since 2020-09-29
	 */
	public final V getValue() {
		return this.value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ (this.getUnitlike() == null ? 0 : this.getUnitlike().hashCode());
		result = prime * result
				+ (this.getValue() == null ? 0 : this.getValue().hashCode());
		return result;
	}

	@Override
	public String toString() {
		final Optional<String> primaryName = this.getUnitlike().getPrimaryName();
		final Optional<String> symbol = this.getUnitlike().getSymbol();
		if (primaryName.isEmpty() && symbol.isEmpty()) {
			final double baseValue = this.getUnitlike()
					.convertToBase(this.getValue());
			return String.format("%s unnamed unit (= %s %s)", this.getValue(),
					baseValue, this.getUnitlike().getBase());
		} else {
			final String unitName = symbol.orElse(primaryName.get());
			return this.getValue() + " " + unitName;
		}
	}
}
