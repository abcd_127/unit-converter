/**
 * Copyright (C) 2022 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnitsGUI;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static sevenUnitsGUI.PrefixSearchRule.COMMON_PREFIXES;
import static sevenUnitsGUI.PrefixSearchRule.NO_PREFIXES;
import static sevenUnitsGUI.PrefixSearchRule.getCoherentOnlyRule;
import static sevenUnitsGUI.PrefixSearchRule.getUniversalRule;

import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.Test;

import sevenUnits.unit.BritishImperial;
import sevenUnits.unit.LinearUnit;
import sevenUnits.unit.Metric;

/**
 * Tests for {@link PrefixSearchRule}
 *
 * @since v0.4.0
 * @since 2022-07-17
 */
class PrefixSearchTest {
	/**
	 * A method that creates duplicate copies of the common prefix rule.
	 */
	private static final PrefixSearchRule getCommonRuleCopy() {
		return getCoherentOnlyRule(Set.of(Metric.KILO, Metric.MILLI));
	}

	/**
	 * Test method for
	 * {@link sevenUnitsGUI.PrefixSearchRule#apply(java.util.Map.Entry)}, for a
	 * coherent unit and {@link PrefixSearchRule#COMMON_PREFIXES}.
	 *
	 * @since v0.4.0
	 * @since 2022-07-17
	 */
	@Test
	final void testCoherentPrefixSearch() {
		final var expected = Map.of("metre", Metric.METRE, "kilometre",
				Metric.KILOMETRE, "millimetre", Metric.MILLIMETRE);
		final var actual = COMMON_PREFIXES
				.apply(Map.entry("metre", Metric.METRE));

		assertEquals(expected, actual,
				"Prefixes not correctly applied to coherent unit.");
	}

	/**
	 * Test method for
	 * {@link sevenUnitsGUI.PrefixSearchRule#equals(java.lang.Object)}.
	 *
	 * @since v0.4.0
	 * @since 2022-07-17
	 */
	@Test
	final void testEquals() {
		assertEquals(getCommonRuleCopy(), getCommonRuleCopy(),
				"equals considers something other than prefixes/rule");

		assertNotEquals(getCoherentOnlyRule(Set.of()), getUniversalRule(Set.of()),
				"equals ignores rule");
	}

	/**
	 * Test method for {@link sevenUnitsGUI.PrefixSearchRule#getPrefixes()}.
	 *
	 * @since v0.4.0
	 * @since 2022-07-17
	 */
	@Test
	final void testGetPrefixes() {
		assertEquals(Set.of(), NO_PREFIXES.getPrefixes());
		assertEquals(Metric.ALL_PREFIXES,
				PrefixSearchRule.ALL_METRIC_PREFIXES.getPrefixes());
	}

	/**
	 * Test method for {@link sevenUnitsGUI.PrefixSearchRule#hashCode()}.
	 *
	 * @since v0.4.0
	 * @since 2022-07-17
	 */
	@Test
	final void testHashCode() {
		assertEquals(getCommonRuleCopy().hashCode(),
				getCommonRuleCopy().hashCode());
	}

	/**
	 * Tests prefix searching for a non-coherent unit and
	 * {@link PrefixSearchRule#COMMON_PREFIXES}.
	 * 
	 * @since v0.4.0
	 * @since 2022-07-17
	 */
	@Test
	final void testNonCoherentPrefixSearch() {
		final var input = Map.entry("inch", BritishImperial.Length.INCH);
		final var expected = Map.ofEntries(input);
		final var actual = COMMON_PREFIXES.apply(input);

		assertEquals(expected, actual, "Prefixes applied to non-coherent unit.");
	}

	/**
	 * Tests that {@link PrefixSearchRule#NO_PREFIXES} returns the original unit.
	 * 
	 * @since v0.4.0
	 * @since 2022-07-17
	 */
	@Test
	void testNoPrefixes() {
		for (final String name : Set.of("name1", "hello", "there")) {
			for (final LinearUnit unit : Set.of(Metric.METRE, Metric.KILOMETRE,
					BritishImperial.Length.INCH)) {
				final var testEntry = Map.entry(name, unit);
				final var expected = Map.ofEntries(testEntry);
				final var actual = NO_PREFIXES.apply(testEntry);
				assertEquals(expected, actual, () -> String
						.format("NO_PREFIXES.apply(%s) != %s", testEntry, actual));
			}
		}
	}

	/**
	 * Test method for {@link sevenUnitsGUI.PrefixSearchRule#toString()}.
	 *
	 * @since v0.4.0
	 * @since 2022-07-17
	 */
	@Test
	final void testToString() {
		assertEquals(
				"Apply the following prefixes: [kilo (\u00D7 1000.0), milli (\u00D7 0.001)]",
				COMMON_PREFIXES.toString());
	}

}
