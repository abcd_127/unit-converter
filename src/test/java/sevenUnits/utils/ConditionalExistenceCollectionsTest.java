/**
 * Copyright (C) 2019 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnits.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.Test;

import sevenUnits.utils.ConditionalExistenceCollections.ConditionalExistenceIterator;

/**
 * Tests the {@link #ConditionalExistenceCollections}. Specifically, it runs
 * normal operations on conditional existence collections and ensures that
 * elements that do not pass the existence condition are not included in the
 * results.
 * 
 * @author Adrien Hopkins
 * @since 2019-10-16
 */
class ConditionalExistenceCollectionsTest {

	/**
	 * The returned iterator ignores elements that don't start with "a".
	 * 
	 * @return test iterator
	 * @since 2019-10-17
	 */
	ConditionalExistenceIterator<String> getTestIterator() {
		final List<String> items = Arrays.asList("aa", "ab", "ba");
		final Iterator<String> it = items.iterator();
		final ConditionalExistenceIterator<String> cit = (ConditionalExistenceIterator<String>) ConditionalExistenceCollections
				.conditionalExistenceIterator(it, s -> s.startsWith("a"));
		return cit;
	}

	/**
	 * The returned map ignores mappings where the value is zero.
	 * 
	 * @return map to be used for test data
	 * @since 2019-10-16
	 */
	Map<String, Integer> getTestMap() {
		final Map<String, Integer> map = new HashMap<>();
		map.put("one", 1);
		map.put("two", 2);
		map.put("zero", 0);
		map.put("ten", 10);
		final Map<String, Integer> conditionalMap = ConditionalExistenceCollections
				.conditionalExistenceMap(map,
						e -> !Integer.valueOf(0).equals(e.getValue()));
		return conditionalMap;
	}

	/**
	 * Test method for the ConditionalExistenceMap's containsKey method.
	 */
	@Test
	void testContainsKeyObject() {
		final Map<String, Integer> map = this.getTestMap();
		assertTrue(map.containsKey("one"));
		assertTrue(map.containsKey("ten"));
		assertFalse(map.containsKey("five"));
		assertFalse(map.containsKey("zero"));
	}

	/**
	 * Test method for the ConditionalExistenceMap's containsValue method.
	 */
	@Test
	void testContainsValueObject() {
		final Map<String, Integer> map = this.getTestMap();
		assertTrue(map.containsValue(1));
		assertTrue(map.containsValue(10));
		assertFalse(map.containsValue(5));
		assertFalse(map.containsValue(0));
	}

	/**
	 * Test method for the ConditionalExistenceMap's entrySet method.
	 */
	@Test
	void testEntrySet() {
		final Map<String, Integer> map = this.getTestMap();
		for (final Entry<String, Integer> e : map.entrySet()) {
			assertTrue(e.getValue() != 0);
		}
	}

	/**
	 * Test method for the ConditionalExistenceMap's get method.
	 */
	@Test
	void testGetObject() {
		final Map<String, Integer> map = this.getTestMap();
		assertEquals(1, map.get("one"));
		assertEquals(10, map.get("ten"));
		assertEquals(null, map.get("five"));
		assertEquals(null, map.get("zero"));
	}

	/**
	 * Test method for the ConditionalExistenceCollection's iterator.
	 */
	@Test
	void testIterator() {
		final ConditionalExistenceIterator<String> testIterator = this
				.getTestIterator();

		assertTrue(testIterator.hasNext);
		assertTrue(testIterator.hasNext());
		assertEquals("aa", testIterator.nextElement);
		assertEquals("aa", testIterator.next());

		assertTrue(testIterator.hasNext);
		assertTrue(testIterator.hasNext());
		assertEquals("ab", testIterator.nextElement);
		assertEquals("ab", testIterator.next());

		assertFalse(testIterator.hasNext);
		assertFalse(testIterator.hasNext());
		assertEquals(null, testIterator.nextElement);
		assertThrows(NoSuchElementException.class, testIterator::next);
	}

	/**
	 * Test method for the ConditionalExistenceMap's keySet operation.
	 */
	@Test
	void testKeySet() {
		final Map<String, Integer> map = this.getTestMap();
		assertFalse(map.keySet().contains("zero"));
	}

	/**
	 * Test method for the ConditionalExistenceMap's values operation.
	 */
	@Test
	void testValues() {
		final Map<String, Integer> map = this.getTestMap();
		assertFalse(map.values().contains(0));
	}

}
