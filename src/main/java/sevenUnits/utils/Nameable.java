/**
 * Copyright (C) 2020 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnits.utils;

import java.util.Optional;
import java.util.Set;

/**
 * An object that can hold one or more names, and possibly a symbol. The name
 * and symbol data should be immutable.
 *
 * @since 2020-09-07
 */
public interface Nameable {
	/**
	 * @return a name for the object - if there's a primary name, it's that,
	 *         otherwise the symbol, otherwise "Unnamed"
	 * @since 2022-02-26
	 */
	default String getName() {
		final NameSymbol ns = this.getNameSymbol();
		return ns.getPrimaryName().or(ns::getSymbol).orElse("Unnamed");
	}

	/**
	 * @return a {@code NameSymbol} that contains this object's primary name,
	 *         symbol and other names
	 * @since 2020-09-07
	 */
	NameSymbol getNameSymbol();

	/**
	 * @return set of alternate names
	 * @since 2020-09-07
	 */
	default Set<String> getOtherNames() {
		return this.getNameSymbol().getOtherNames();
	}

	/**
	 * @return preferred name of object
	 * @since 2020-09-07
	 */
	default Optional<String> getPrimaryName() {
		return this.getNameSymbol().getPrimaryName();
	}

	/**
	 * @return a short name for the object - if there's a symbol, it's that,
	 *         otherwise the symbol, otherwise "Unnamed"
	 * @since 2022-02-26
	 */
	default String getShortName() {
		final NameSymbol ns = this.getNameSymbol();
		return ns.getSymbol().or(ns::getPrimaryName).orElse("Unnamed");
	}

	/**
	 * @return short symbol representing object
	 * @since 2020-09-07
	 */
	default Optional<String> getSymbol() {
		return this.getNameSymbol().getSymbol();
	}
}
