/**
 * Copyright (C) 2021-2024 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnits;

import sevenUnits.utils.SemanticVersionNumber;

/**
 * Information about 7Units
 * 
 * @since 0.3.1
 * @since 2021-06-28
 */
public final class ProgramInfo {

	/** The version number (0.5.0) */
	public static final SemanticVersionNumber VERSION = SemanticVersionNumber
			.stableVersion(0, 5, 0);

	private ProgramInfo() {
		// this class is only for static variables, you shouldn't be able to
		// construct an instance
		throw new AssertionError();
	}

}
