/**
 * Copyright (C) 2019 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnits.unit;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import sevenUnits.utils.NameSymbol;

/**
 * A unit that other units are defined by.
 * <p>
 * Note that BaseUnits <b>must</b> have names and symbols. This is because they
 * are used for toString code. Therefore, the Optionals provided by
 * {@link #getPrimaryName} and {@link #getSymbol} will always contain a value.
 * 
 * @author Adrien Hopkins
 * @since 2019-10-16
 */
public final class BaseUnit extends Unit {
	/**
	 * Gets a base unit from the dimension it measures, its name and its symbol.
	 * 
	 * @param dimension dimension measured by this unit
	 * @param name      name of unit
	 * @param symbol    symbol of unit
	 * @return base unit
	 * @since 2019-10-16
	 */
	public static BaseUnit valueOf(final BaseDimension dimension,
			final String name, final String symbol) {
		return new BaseUnit(dimension, name, symbol, new HashSet<>());
	}

	/**
	 * Gets a base unit from the dimension it measures, its name and its symbol.
	 * 
	 * @param dimension dimension measured by this unit
	 * @param name      name of unit
	 * @param symbol    symbol of unit
	 * @return base unit
	 * @since 2019-10-21
	 */
	public static BaseUnit valueOf(final BaseDimension dimension,
			final String name, final String symbol, final Set<String> otherNames) {
		return new BaseUnit(dimension, name, symbol, otherNames);
	}

	/**
	 * The dimension measured by this base unit.
	 */
	private final BaseDimension dimension;

	/**
	 * Creates the {@code BaseUnit}.
	 * 
	 * @param dimension   dimension of unit
	 * @param primaryName name of unit
	 * @param symbol      symbol of unit
	 * @throws NullPointerException if any argument is null
	 * @since 2019-10-16
	 */
	private BaseUnit(final BaseDimension dimension, final String primaryName,
			final String symbol, final Set<String> otherNames) {
		super(NameSymbol.of(primaryName, symbol, otherNames));
		this.dimension = Objects.requireNonNull(dimension,
				"dimension must not be null.");
	}

	/**
	 * Returns a {@code LinearUnit} with this unit as a base and a conversion
	 * factor of 1. This operation must be done in order to allow units to be
	 * created with operations.
	 * 
	 * @return this unit as a {@code LinearUnit}
	 * @since 2019-10-16
	 */
	public LinearUnit asLinearUnit() {
		return LinearUnit.valueOf(this.getBase(), 1);
	}

	@Override
	protected double convertFromBase(final double value) {
		return value;
	}

	@Override
	protected double convertToBase(final double value) {
		return value;
	}

	/**
	 * @return dimension
	 * @since 2019-10-16
	 */
	public final BaseDimension getBaseDimension() {
		return this.dimension;
	}

	@Override
	public String toString() {
		return this.getPrimaryName().orElse("Unnamed unit")
				+ (this.getSymbol().isPresent()
						? String.format(" (%s)", this.getSymbol().get())
						: "");
	}

	@Override
	public BaseUnit withName(final NameSymbol ns) {
		Objects.requireNonNull(ns, "ns must not be null.");
		if (!ns.getPrimaryName().isPresent())
			throw new IllegalArgumentException(
					"BaseUnits must have primary names.");
		if (!ns.getSymbol().isPresent())
			throw new IllegalArgumentException("BaseUnits must have symbols.");
		return BaseUnit.valueOf(this.getBaseDimension(),
				ns.getPrimaryName().get(), ns.getSymbol().get(),
				ns.getOtherNames());
	}
}
