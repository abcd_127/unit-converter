/**
 * Copyright (C) 2019 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnits.unit;

import sevenUnits.utils.NameSymbol;

/**
 * A static utility class that contains units in the British Imperial system.
 * 
 * @author Adrien Hopkins
 * @since 2019-10-21
 */
public final class BritishImperial {
	/**
	 * Imperial units that measure area
	 * 
	 * @author Adrien Hopkins
	 * @since 2019-11-08
	 */
	public static final class Area {
		public static final LinearUnit SQUARE_FOOT = Length.FOOT.toExponent(2);
		public static final LinearUnit SQUARE_YARD = Length.YARD.toExponent(2);
		public static final LinearUnit SQUARE_MILE = Length.MILE.toExponent(2);
		public static final LinearUnit PERCH = Length.ROD.times(Length.ROD);
		public static final LinearUnit ROOD = Length.ROD.times(Length.FURLONG);
		public static final LinearUnit ACRE = Length.FURLONG.times(Length.CHAIN);
	}

	/**
	 * Imperial units that measure length
	 * 
	 * @author Adrien Hopkins
	 * @since 2019-10-28
	 */
	public static final class Length {
		/**
		 * According to the International Yard and Pound of 1959, a yard is
		 * defined as exactly 0.9144 metres.
		 */
		public static final LinearUnit YARD = Metric.METRE.times(0.9144);
		public static final LinearUnit FOOT = YARD.dividedBy(3);
		public static final LinearUnit INCH = FOOT.dividedBy(12);
		public static final LinearUnit THOU = INCH.dividedBy(1000);
		public static final LinearUnit CHAIN = YARD.times(22);
		public static final LinearUnit FURLONG = CHAIN.times(10);
		public static final LinearUnit MILE = FURLONG.times(8);
		public static final LinearUnit LEAGUE = MILE.times(3);

		public static final LinearUnit NAUTICAL_MILE = Metric.METRE.times(1852);
		public static final LinearUnit CABLE = NAUTICAL_MILE.dividedBy(10);
		public static final LinearUnit FATHOM = CABLE.dividedBy(100);

		public static final LinearUnit ROD = YARD.times(5.5);
		public static final LinearUnit LINK = ROD.dividedBy(25);
	}

	/**
	 * British Imperial units that measure mass.
	 * 
	 * @author Adrien Hopkins
	 * @since 2019-11-08
	 */
	public static final class Mass {
		public static final LinearUnit POUND = Metric.GRAM.times(453.59237);
		public static final LinearUnit OUNCE = POUND.dividedBy(16);
		public static final LinearUnit DRACHM = POUND.dividedBy(256);
		public static final LinearUnit GRAIN = POUND.dividedBy(7000);
		public static final LinearUnit STONE = POUND.times(14);
		public static final LinearUnit QUARTER = STONE.times(2);
		public static final LinearUnit HUNDREDWEIGHT = QUARTER.times(4);
		public static final LinearUnit LONG_TON = HUNDREDWEIGHT.times(20);
		public static final LinearUnit SLUG = Metric.KILOGRAM.times(14.59390294);
	}

	/**
	 * British Imperial units that measure volume
	 * 
	 * @author Adrien Hopkins
	 * @since 2019-11-08
	 */
	public static final class Volume {
		public static final LinearUnit FLUID_OUNCE = Metric.LITRE
				.withPrefix(Metric.MILLI).times(28.4130625);
		public static final LinearUnit GILL = FLUID_OUNCE.times(5);
		public static final LinearUnit PINT = FLUID_OUNCE.times(20);
		public static final LinearUnit QUART = PINT.times(2);
		public static final LinearUnit GALLON = QUART.times(4);
		public static final LinearUnit PECK = GALLON.times(2);
		public static final LinearUnit BUSHEL = PECK.times(4);

		public static final LinearUnit CUBIC_INCH = Length.INCH.toExponent(3);
		public static final LinearUnit CUBIC_FOOT = Length.FOOT.toExponent(3);
		public static final LinearUnit CUBIC_YARD = Length.YARD.toExponent(3);
		public static final LinearUnit ACRE_FOOT = Area.ACRE.times(Length.FOOT);
	}

	public static final LinearUnit OUNCE_FORCE = Mass.OUNCE
			.times(Metric.Constants.EARTH_GRAVITY);
	public static final LinearUnit POUND_FORCE = Mass.POUND
			.times(Metric.Constants.EARTH_GRAVITY);

	public static final LinearUnit BRITISH_THERMAL_UNIT = Metric.JOULE
			.times(1055.06);
	public static final LinearUnit CALORIE = Metric.JOULE.times(4.184);
	public static final LinearUnit KILOCALORIE = Metric.JOULE.times(4184);

	public static final Unit FAHRENHEIT = Unit
			.fromConversionFunctions(Metric.KELVIN.getBase(),
					tempK -> tempK * 1.8 - 459.67, tempF -> (tempF + 459.67) / 1.8)
			.withName(NameSymbol.of("degree Fahrenheit", "\u00B0F"));
}
