/**
 * Copyright (C) 2018 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnits.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static sevenUnits.unit.Metric.Dimensions.AREA;
import static sevenUnits.unit.Metric.Dimensions.ENERGY;
import static sevenUnits.unit.Metric.Dimensions.LENGTH;
import static sevenUnits.unit.Metric.Dimensions.MASS;
import static sevenUnits.unit.Metric.Dimensions.MASS_DENSITY;
import static sevenUnits.unit.Metric.Dimensions.QUANTITY;
import static sevenUnits.unit.Metric.Dimensions.TIME;
import static sevenUnits.unit.Metric.Dimensions.VOLUME;

import org.junit.jupiter.api.Test;

import sevenUnits.unit.Metric;

/**
 * Tests for {@link ObjectProduct} using BaseDimension as a test object. This is
 * NOT part of this program's public API.
 * 
 * @author Adrien Hopkins
 * @since 2018-12-12
 * @since v0.1.0
 */
class ObjectProductTest {
	/**
	 * Tests {@link UnitDimension#equals}
	 * 
	 * @since 2018-12-12
	 * @since v0.1.0
	 */
	@Test
	public void testEquals() {
		assertEquals(LENGTH, LENGTH);
		assertFalse(LENGTH.equals(QUANTITY));
	}

	/**
	 * Tests {@code UnitDimension}'s exponentiation
	 * 
	 * @since 2019-01-15
	 * @since v0.1.0
	 */
	@Test
	public void testExponents() {
		assertEquals(1, LENGTH.getExponent(Metric.BaseDimensions.LENGTH));
		assertEquals(3, VOLUME.getExponent(Metric.BaseDimensions.LENGTH));
	}

	/**
	 * Tests {@code UnitDimension}'s multiplication and division.
	 * 
	 * @since 2018-12-12
	 * @since v0.1.0
	 */
	@Test
	public void testMultiplicationAndDivision() {
		assertEquals(AREA, LENGTH.times(LENGTH));
		assertEquals(MASS_DENSITY, MASS.dividedBy(VOLUME));
		assertEquals(ENERGY, AREA.times(MASS).dividedBy(TIME).dividedBy(TIME));
		assertEquals(LENGTH, LENGTH.times(TIME).dividedBy(TIME));
	}
}
