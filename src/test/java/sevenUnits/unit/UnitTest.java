/**
 * Copyright (C) 2018 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnits.unit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.RoundingMode;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.Test;

import sevenUnits.utils.DecimalComparison;
import sevenUnits.utils.NameSymbol;
import sevenUnits.utils.UncertainDouble;

/**
 * Testing the various Unit classes. This is NOT part of this program's public
 * API.
 * 
 * @author Adrien Hopkins
 * @since 2018-12-22
 * @since v0.1.0
 */
class UnitTest {
	/** A random number generator */
	private static final Random rng = ThreadLocalRandom.current();

	@Test
	public void testAdditionAndSubtraction() {
		final LinearUnit inch = Metric.METRE.times(0.0254)
				.withName(NameSymbol.of("inch", "in"));
		final LinearUnit foot = Metric.METRE.times(0.3048)
				.withName(NameSymbol.of("foot", "ft"));

		assertEquals(inch.plus(foot), Metric.METRE.times(0.3302));
		assertEquals(foot.minus(inch), Metric.METRE.times(0.2794));

		// test with LinearUnitValue
		final LinearUnitValue value1 = LinearUnitValue.getExact(Metric.METRE, 15);
		final LinearUnitValue value2 = LinearUnitValue.getExact(foot, 120);
		final LinearUnitValue value3 = LinearUnitValue.getExact(Metric.METRE,
				0.5);
		final LinearUnitValue value4 = LinearUnitValue.getExact(Metric.KILOGRAM,
				60);

		// make sure addition is done correctly
		assertEquals(51.576, value1.plus(value2).getValueExact(), 0.001);
		assertEquals(15.5, value1.plus(value3).getValueExact());
		assertEquals(52.076, value1.plus(value2).plus(value3).getValueExact(),
				0.001);

		// make sure addition uses the correct unit, and is still associative
		// (ignoring floating-point rounding errors)
		assertEquals(Metric.METRE, value1.plus(value2).getUnit());
		assertEquals(Metric.METRE, value1.plus(value2).plus(value3).getUnit());
		assertEquals(foot, value2.plus(value1).getUnit());
		assertTrue(value1.plus(value2).equals(value2.plus(value1), true));

		// make sure errors happen when they should
		assertThrows(IllegalArgumentException.class, () -> value1.plus(value4));
		assertThrows(IllegalArgumentException.class, () -> value1.minus(value4));
	}

	@Test
	public void testConversion() {
		final LinearUnit metre = Metric.METRE;
		final Unit inch = metre.times(0.0254);

		final UnitValue value = UnitValue.of(inch, 75);

		assertEquals(1.9, inch.convertTo(metre, 75), 0.01);
		assertEquals(1.9, value.convertTo(metre).getValue(), 0.01);

		// try random stuff
		for (int i = 0; i < 1000; i++) {
			// initiate random values
			final double conversionFactor = UnitTest.rng.nextDouble() * 1000000;
			final double testValue = UnitTest.rng.nextDouble() * 1000000;
			final double expected = testValue * conversionFactor;

			// test
			final Unit unit = Metric.METRE.times(conversionFactor);
			final double actual = unit.convertToBase(testValue);

			assertEquals(actual, expected,
					expected * DecimalComparison.DOUBLE_EPSILON);
		}
	}

	@Test
	public void testEquals() {
		final LinearUnit metre = Metric.METRE;
		final Unit meter = Metric.BaseUnits.METRE.asLinearUnit();

		assertEquals(metre, meter);
	}

	@Test
	public void testIsMetric() {
		final Unit metre = Metric.METRE;
		final Unit megasecond = Metric.SECOND.withPrefix(Metric.MEGA);
		final Unit hour = Metric.HOUR;

		assertTrue(metre.isMetric());
		assertTrue(megasecond.isMetric());
		assertFalse(hour.isMetric());
	}

	@Test
	public void testMultiplicationAndDivision() {
		// test unit-times-unit multiplication
		final LinearUnit generatedJoule = Metric.KILOGRAM
				.times(Metric.METRE.toExponent(2))
				.dividedBy(Metric.SECOND.toExponent(2));
		final LinearUnit actualJoule = Metric.JOULE;

		assertEquals(generatedJoule, actualJoule);

		// test multiplication by conversion factors
		final LinearUnit kilometre = Metric.METRE.times(1000);
		final LinearUnit hour = Metric.SECOND.times(3600);
		final LinearUnit generatedKPH = kilometre.dividedBy(hour);

		final LinearUnit actualKPH = Metric.METRE.dividedBy(Metric.SECOND)
				.dividedBy(3.6);

		assertEquals(generatedKPH, actualKPH);
	}

	@Test
	public void testPrefixes() {
		final LinearUnit generatedKilometre = Metric.METRE
				.withPrefix(Metric.KILO);
		final LinearUnit actualKilometre = Metric.METRE.times(1000);

		assertEquals(generatedKilometre, actualKilometre);
	}

	/**
	 * Tests converting an uncertain LinearUnitValue to a string.
	 * 
	 * @since 2021-11-04
	 */
	@Test
	public void testValueToString1() {
		final LinearUnitValue value = LinearUnitValue.of(Metric.METRE,
				UncertainDouble.of(10, 0.24));

		assertEquals("(10.0 � 0.2) m", value.toString());
		assertEquals("(10.0 � 0.2) m",
				value.toString(true, RoundingMode.HALF_EVEN));
		assertEquals("10.0 m", value.toString(false, RoundingMode.HALF_EVEN));
	}

	/**
	 * Tests converting a certain LinearUnitValue to a string.
	 * 
	 * @since 2021-11-04
	 */
	@Test
	public void testValueToString2() {
		final LinearUnitValue value = LinearUnitValue.of(Metric.METRE,
				UncertainDouble.of(10, 0));

		assertEquals("10.0 m", value.toString());
		assertEquals("(10.0 � 0.0) m",
				value.toString(true, RoundingMode.HALF_EVEN));
		assertEquals("10.0 m", value.toString(false, RoundingMode.HALF_EVEN));
	}

	/**
	 * Tests converting an unnamed LinearUnitValue to a string.
	 * 
	 * @since 2021-11-04
	 */
	@Test
	public void testValueToString3() {
		final LinearUnitValue value = LinearUnitValue.of(
				Metric.METRE.withName(NameSymbol.EMPTY),
				UncertainDouble.of(10, 0.24));

		assertEquals("10.0 unnamed unit (= 10.0 m)",
				value.toString(false, RoundingMode.HALF_EVEN));
	}

	/**
	 * Tests converting a named UnitValue to a string.
	 * 
	 * @since 2021-11-04
	 */
	@Test
	public void testValueToString4() {
		final UnitValue value = UnitValue.of(BritishImperial.FAHRENHEIT, 80);

		assertEquals("80.0 \u00B0F", value.toString());
	}

	/**
	 * Tests converting an unnamed UnitValue to a string.
	 * 
	 * @since 2021-11-04
	 */
	@Test
	public void testValueToString5() {
		final UnitValue value = UnitValue
				.of(USCustomary.FAHRENHEIT.withName(NameSymbol.EMPTY), 50);

		assertEquals("50.0 unnamed unit (= 283.15 K)", value.toString());
	}
}
