/**
 * Copyright (C) 2022 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnits.unit;

import java.util.function.Predicate;

/**
 * A type of unit, as chosen by the type of system it is in.
 * <ul>
 * <li>{@code METRIC} refers to metric/SI units that pass {@link Unit#isMetric}
 * <li>{@code SEMI_METRIC} refers to the degree Celsius (which is an official SI
 * unit but does not pass {@link Unit#isMetric}) and non-metric units intended
 * for use with the SI.
 * <li>{@code NON_METRIC} refers to units that are neither metric nor intended
 * for use with the metric system (e.g. imperial and customary units)
 * </ul>
 *
 * @since 2022-04-10
 */
public enum UnitType {
	METRIC, SEMI_METRIC, NON_METRIC;

	/**
	 * Determines which type a unit is. The type will be:
	 * <ul>
	 * <li>{@code SEMI_METRIC} if the unit passes the provided predicate
	 * <li>{@code METRIC} if it fails the predicate but is metric
	 * <li>{@code NON_METRIC} if it fails the predicate and is not metric
	 * </ul>
	 *
	 * @param u            unit to test
	 * @param isSemiMetric predicate to determine if a unit is semi-metric
	 * @return type of unit
	 * @since 2022-04-18
	 */
	public static final UnitType getType(Unit u, Predicate<Unit> isSemiMetric) {
		if (isSemiMetric.test(u))
			return SEMI_METRIC;
		else if (u.isMetric())
			return METRIC;
		else
			return NON_METRIC;
	}
}
