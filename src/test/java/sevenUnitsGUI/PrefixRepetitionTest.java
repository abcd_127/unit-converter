/**
 * Copyright (C) 2022 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnitsGUI;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static sevenUnitsGUI.DefaultPrefixRepetitionRule.COMPLEX_REPETITION;
import static sevenUnitsGUI.DefaultPrefixRepetitionRule.NO_REPETITION;
import static sevenUnitsGUI.DefaultPrefixRepetitionRule.NO_RESTRICTION;

import java.util.List;

import org.junit.jupiter.api.Test;

import sevenUnits.unit.Metric;

/**
 * Tests for the default prefix repetition rules.
 *
 * @since v0.4.0
 * @since 2022-07-17
 */
class PrefixRepetitionTest {
	/**
	 * Ensures that the complex repetition rule disallows invalid prefix lists.
	 * 
	 * @since v0.4.0
	 * @since 2022-07-17
	 */
	@Test
	void testInvalidComplexRepetition() {
		assertFalse(COMPLEX_REPETITION.test(List.of(Metric.KILO, Metric.YOTTA)),
				"Complex repetition does not factor order of prefixes");
		assertFalse(COMPLEX_REPETITION.test(List.of(Metric.MEGA, Metric.KILO)),
				"\"kilomega\" allowed (should use \"giga\")");
		assertFalse(
				COMPLEX_REPETITION
						.test(List.of(Metric.YOTTA, Metric.MEGA, Metric.KILO)),
				"\"kilomega\" allowed after yotta (should use \"giga\")");
		assertFalse(COMPLEX_REPETITION.test(List.of(Metric.YOTTA, Metric.MILLI)),
				"Complex repetition does not factor direction of prefixes");
	}

	/**
	 * Tests the {@code NO_REPETITION} rule.
	 * 
	 * @since v0.4.0
	 * @since 2022-07-17
	 */
	@Test
	void testNoRepetition() {
		assertTrue(NO_REPETITION.test(List.of(Metric.KILO)));
		assertFalse(NO_REPETITION.test(List.of(Metric.KILO, Metric.YOTTA)));
		assertTrue(NO_REPETITION.test(List.of(Metric.MILLI)));
		assertTrue(NO_REPETITION.test(List.of()), "Empty list yields false");
	}

	/**
	 * Tests the {@code NO_RESTRICTION} rule.
	 * 
	 * @since v0.4.0
	 * @since 2022-07-17
	 */
	@Test
	void testNoRestriction() {
		assertTrue(NO_RESTRICTION.test(List.of(Metric.KILO)));
		assertTrue(NO_RESTRICTION.test(List.of(Metric.KILO, Metric.YOTTA)));
		assertTrue(NO_RESTRICTION.test(List.of(Metric.MILLI)));
		assertTrue(NO_RESTRICTION.test(List.of()));
	}

	/**
	 * Ensures that the complex repetition rule allows valid prefix lists.
	 * 
	 * @since v0.4.0
	 * @since 2022-07-17
	 */
	@Test
	void testValidComplexRepetition() {
		// simple situations
		assertTrue(COMPLEX_REPETITION.test(List.of(Metric.KILO)),
				"Single prefix not allowed");
		assertTrue(COMPLEX_REPETITION.test(List.of()), "No prefixes not allowed");

		// valid complex repetition
		assertTrue(COMPLEX_REPETITION.test(List.of(Metric.YOTTA, Metric.KILO)),
				"Valid complex repetition (kiloyotta) not allowed");
		assertTrue(COMPLEX_REPETITION.test(List.of(Metric.KILO, Metric.DEKA)),
				"Valid complex repetition (dekakilo) not allowed");
		assertTrue(
				COMPLEX_REPETITION
						.test(List.of(Metric.YOTTA, Metric.KILO, Metric.DEKA)),
				"Valid complex repetition (dekakiloyotta) not allowed");
		assertTrue(
				COMPLEX_REPETITION.test(List.of(Metric.YOTTA, Metric.YOTTA,
						Metric.KILO, Metric.DEKA)),
				"Valid complex repetition (dekakiloyottayotta) not allowed");

		// valid with negative prefixes
		assertTrue(COMPLEX_REPETITION.test(List.of(Metric.YOCTO, Metric.MILLI)),
				"Valid complex repetition (milliyocto) not allowed");
		assertTrue(COMPLEX_REPETITION.test(List.of(Metric.MILLI, Metric.CENTI)),
				"Valid complex repetition (centimilli) not allowed");
		assertTrue(
				COMPLEX_REPETITION
						.test(List.of(Metric.YOCTO, Metric.MILLI, Metric.CENTI)),
				"Valid complex repetition (centimilliyocto) not allowed");
	}
}
