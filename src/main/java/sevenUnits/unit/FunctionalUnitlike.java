/**
 * Copyright (C) 2020 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnits.unit;

import java.util.function.DoubleFunction;
import java.util.function.ToDoubleFunction;

import sevenUnits.utils.NameSymbol;
import sevenUnits.utils.ObjectProduct;

/**
 * A unitlike form that converts using two conversion functions.
 *
 * @since 2020-09-07
 */
final class FunctionalUnitlike<V> extends Unitlike<V> {
	/**
	 * A function that accepts a value in the unitlike form's base and returns a
	 * value in the unitlike form.
	 * 
	 * @since 2020-09-07
	 */
	private final DoubleFunction<V> converterFrom;

	/**
	 * A function that accepts a value in the unitlike form and returns a value
	 * in the unitlike form's base.
	 */
	private final ToDoubleFunction<V> converterTo;

	/**
	 * Creates the {@code FunctionalUnitlike}.
	 * 
	 * @param base          unitlike form's base
	 * @param converterFrom function that accepts a value in the unitlike form's
	 *                      base and returns a value in the unitlike form.
	 * @param converterTo   function that accepts a value in the unitlike form
	 *                      and returns a value in the unitlike form's base.
	 * @throws NullPointerException if any argument is null
	 * @since 2019-05-22
	 */
	protected FunctionalUnitlike(ObjectProduct<BaseUnit> unitBase, NameSymbol ns,
			DoubleFunction<V> converterFrom, ToDoubleFunction<V> converterTo) {
		super(unitBase, ns);
		this.converterFrom = converterFrom;
		this.converterTo = converterTo;
	}

	@Override
	protected V convertFromBase(double value) {
		return this.converterFrom.apply(value);
	}

	@Override
	protected double convertToBase(V value) {
		return this.converterTo.applyAsDouble(value);
	}

}
