/**
 * Copyright (C) 2022 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnitsGUI;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Test for the TabbedView
 *
 * @since v0.4.0
 * @since 2022-07-17
 */
class TabbedViewTest {
	/**
	 * @return a view with all settings set to standard values
	 * 
	 * @since v0.4.0
	 * @since 2022-07-17
	 */
	private static final TabbedView setupView() {
		final var view = new TabbedView();
		final var presenter = view.getPresenter();

		presenter.setNumberDisplayRule(StandardDisplayRules.uncertaintyBased());
		presenter.setPrefixRepetitionRule(
				DefaultPrefixRepetitionRule.NO_RESTRICTION);
		presenter.setSearchRule(PrefixSearchRule.COMMON_PREFIXES);
		presenter.setOneWayConversionEnabled(false);
		presenter.setShowDuplicates(true);

		return view;
	}

	/**
	 * Simulates an expression conversion operation, and ensures it works
	 * properly.
	 * 
	 * @since v0.4.0
	 * @since 2022-07-17
	 */
	@Test
	void testExpressionConversion() {
		final var view = setupView();

		// prepare for unit conversion
		view.masterPane.setSelectedIndex(1);
		view.fromEntry.setText("250.0 inch");
		view.toEntry.setText("metre");

		view.convertExpressionButton.doClick();

		// check result of conversion
		assertEquals("250.0 inch = 6.350 metre", view.expressionOutput.getText());
	}

	/**
	 * Simulates a unit conversion operation, and ensures it works properly.
	 * 
	 * @since v0.4.0
	 * @since 2022-07-17
	 */
	@Test
	void testUnitConversion() {
		final var view = setupView();

		// prepare for unit conversion
		view.masterPane.setSelectedIndex(0);
		view.dimensionSelector.setSelectedItem("Length");
		view.fromSearch.getSearchList().setSelectedValue("inch", true);
		view.toSearch.getSearchList().setSelectedValue("metre", true);
		view.valueInput.setText("250.0");

		view.convertUnitButton.doClick();

		// check result of conversion
		assertEquals("250.0 inch = 6.350 metre", view.unitOutput.getText());
	}

}
