/**
 * Copyright (C) 2019 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnits.unit;

/**
 * A static utility class that contains units in the US Customary system.
 * 
 * @author Adrien Hopkins
 * @since 2019-10-21
 */
public final class USCustomary {
	/**
	 * US Customary units that measure area
	 * 
	 * @author Adrien Hopkins
	 * @since 2019-11-08
	 */
	public static final class Area {
		public static final LinearUnit SQUARE_SURVEY_FOOT = Length.SURVEY_FOOT
				.times(Length.SURVEY_FOOT);
		public static final LinearUnit SQUARE_CHAIN = Length.SURVEY_CHAIN
				.times(Length.SURVEY_CHAIN);
		public static final LinearUnit ACRE = Length.SURVEY_CHAIN
				.times(Length.SURVEY_FURLONG);
		public static final LinearUnit SECTION = Length.SURVEY_MILE
				.times(Length.SURVEY_MILE);
		public static final LinearUnit SURVEY_TOWNSHIP = SECTION.times(36);
	}

	/**
	 * US Customary units that measure length
	 * 
	 * @author Adrien Hopkins
	 * @since 2019-10-28
	 */
	public static final class Length {
		public static final LinearUnit FOOT = BritishImperial.Length.FOOT;
		public static final LinearUnit INCH = BritishImperial.Length.INCH;
		public static final LinearUnit HAND = INCH.times(4);
		public static final LinearUnit PICA = INCH.dividedBy(6);
		public static final LinearUnit POINT = PICA.dividedBy(12);
		public static final LinearUnit YARD = BritishImperial.Length.YARD;
		public static final LinearUnit MILE = BritishImperial.Length.MILE;

		public static final LinearUnit SURVEY_FOOT = Metric.METRE
				.times(1200.0 / 3937.0);
		public static final LinearUnit SURVEY_LINK = SURVEY_FOOT
				.times(33.0 / 50.0);
		public static final LinearUnit SURVEY_ROD = SURVEY_FOOT.times(16.5);
		public static final LinearUnit SURVEY_CHAIN = SURVEY_ROD.times(4);
		public static final LinearUnit SURVEY_FURLONG = SURVEY_CHAIN.times(10);
		public static final LinearUnit SURVEY_MILE = SURVEY_FURLONG.times(8);
		public static final LinearUnit SURVEY_LEAGUE = SURVEY_MILE.times(3);

		public static final LinearUnit NAUTICAL_MILE = BritishImperial.Length.NAUTICAL_MILE;
		public static final LinearUnit FATHOM = YARD.times(2);
		public static final LinearUnit CABLE = FATHOM.times(120);
	}

	/**
	 * mass units
	 * 
	 * @author Adrien Hopkins
	 * @since 2019-11-08
	 */
	public static final class Mass {
		public static final LinearUnit GRAIN = BritishImperial.Mass.GRAIN;
		public static final LinearUnit DRAM = BritishImperial.Mass.DRACHM;
		public static final LinearUnit OUNCE = BritishImperial.Mass.OUNCE;
		public static final LinearUnit POUND = BritishImperial.Mass.POUND;
		public static final LinearUnit HUNDREDWEIGHT = POUND.times(100);
		public static final LinearUnit SHORT_TON = HUNDREDWEIGHT.times(20);

		// troy system for precious metals
		public static final LinearUnit PENNYWEIGHT = GRAIN.times(24);
		public static final LinearUnit TROY_OUNCE = PENNYWEIGHT.times(20);
		public static final LinearUnit TROY_POUND = TROY_OUNCE.times(12);
	}

	/**
	 * Volume units
	 * 
	 * @author Adrien Hopkins
	 * @since 2019-11-08
	 */
	public static final class Volume {
		public static final LinearUnit CUBIC_INCH = Length.INCH.toExponent(3);
		public static final LinearUnit CUBIC_FOOT = Length.FOOT.toExponent(3);
		public static final LinearUnit CUBIC_YARD = Length.YARD.toExponent(3);
		public static final LinearUnit ACRE_FOOT = Area.ACRE.times(Length.FOOT);

		public static final LinearUnit MINIM = Metric.LITRE
				.withPrefix(Metric.MICRO).times(61.611519921875);
		public static final LinearUnit FLUID_DRAM = MINIM.times(60);
		public static final LinearUnit TEASPOON = MINIM.times(80);
		public static final LinearUnit TABLESPOON = TEASPOON.times(3);
		public static final LinearUnit FLUID_OUNCE = TABLESPOON.times(2);
		public static final LinearUnit SHOT = TABLESPOON.times(3);
		public static final LinearUnit GILL = FLUID_OUNCE.times(4);
		public static final LinearUnit CUP = GILL.times(2);
		public static final LinearUnit PINT = CUP.times(2);
		public static final LinearUnit QUART = PINT.times(2);
		public static final LinearUnit GALLON = QUART.times(4);
		public static final LinearUnit BARREL = GALLON.times(31.5);
		public static final LinearUnit OIL_BARREL = GALLON.times(42);
		public static final LinearUnit HOGSHEAD = GALLON.times(63);

		public static final LinearUnit DRY_PINT = Metric.LITRE
				.times(0.5506104713575);
		public static final LinearUnit DRY_QUART = DRY_PINT.times(2);
		public static final LinearUnit DRY_GALLON = DRY_QUART.times(4);
		public static final LinearUnit PECK = DRY_GALLON.times(2);
		public static final LinearUnit BUSHEL = PECK.times(4);
		public static final LinearUnit DRY_BARREL = CUBIC_INCH.times(7056);
	}

	public static final LinearUnit OUNCE_FORCE = BritishImperial.OUNCE_FORCE;
	public static final LinearUnit POUND_FORCE = BritishImperial.POUND_FORCE;

	public static final LinearUnit BRITISH_THERMAL_UNIT = BritishImperial.BRITISH_THERMAL_UNIT;
	public static final LinearUnit CALORIE = BritishImperial.CALORIE;
	public static final LinearUnit KILOCALORIE = BritishImperial.KILOCALORIE;
	public static final LinearUnit FOOT_POUND = POUND_FORCE.times(Length.FOOT);

	public static final LinearUnit HORSEPOWER = Length.FOOT.times(POUND_FORCE)
			.dividedBy(Metric.MINUTE).times(33000);
	public static final LinearUnit POUND_PER_SQUARE_INCH = POUND_FORCE
			.dividedBy(Length.INCH.toExponent(2));

	public static final Unit FAHRENHEIT = BritishImperial.FAHRENHEIT;
}
