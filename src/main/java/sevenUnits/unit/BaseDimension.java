/**
 * Copyright (C) 2019, 2022 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnits.unit;

import java.util.Objects;

import sevenUnits.utils.NameSymbol;
import sevenUnits.utils.Nameable;

/**
 * A dimension that defines a {@code BaseUnit}
 * 
 * @author Adrien Hopkins
 * @since 2019-10-16
 */
public final class BaseDimension implements Nameable {
	/**
	 * Gets a {@code BaseDimension} with the provided name and symbol.
	 * 
	 * @param name   name of dimension
	 * @param symbol symbol used for dimension
	 * @return dimension
	 * @since 2019-10-16
	 */
	public static BaseDimension valueOf(final String name, final String symbol) {
		return new BaseDimension(name, symbol);
	}

	/**
	 * The name of the dimension.
	 */
	private final String name;
	/**
	 * The symbol used by the dimension. Symbols should be short, generally one
	 * or two characters.
	 */
	private final String symbol;

	/**
	 * Creates the {@code BaseDimension}.
	 * 
	 * @param name   name of unit
	 * @param symbol symbol of unit
	 * @throws NullPointerException if any argument is null
	 * @since 2019-10-16
	 */
	private BaseDimension(final String name, final String symbol) {
		this.name = Objects.requireNonNull(name, "name must not be null.");
		this.symbol = Objects.requireNonNull(symbol, "symbol must not be null.");
	}

	/**
	 * @since v0.4.0
	 */
	@Override
	public NameSymbol getNameSymbol() {
		return NameSymbol.of(this.name, this.symbol);
	}

	@Override
	public String toString() {
		return String.format("%s (%s)", this.name, this.symbol);
	}
}
