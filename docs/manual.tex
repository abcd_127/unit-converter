% Created 2024-03-24 Sun 13:16
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[a4paper, lmargin=25mm, rmargin=25mm, tmargin=25mm, bmargin=25mm]{geometry}
\date{2024 March 23}
\title{7Units User Manual\\\medskip
\large For Version 0.5.0}
\hypersetup{
 pdfauthor={},
 pdftitle={7Units User Manual},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.2 (Org mode 9.6.15)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents

\newpage
\section{Introduction and Purpose}
\label{sec:orgc09fcc7}
7Units is a program that can be used to convert units.  This document outlines how to use the program.
\section{System Requirements}
\label{sec:orga902335}
\begin{itemize}
\item Works on all major operating systems \\[0pt]
\textbf{NOTE:} All screenshots in this document were taken on Windows 10.  If you use a different operating system, the program will probably look different than what is shown.
\item Java version 11+ required
\end{itemize}

\newpage
\section{How to Use 7Units}
\label{sec:orgdec078f}
\subsection{Simple Unit Conversion}
\label{sec:org785ebcb}
\begin{enumerate}
\item Select the "Convert Units" tab if it is not already selected.  You should see a screen like in figure \ref{main-interface-dimension}:
\begin{figure}[htbp]
\centering
\includegraphics[height=250px]{../screenshots/main-interface-dimension-converter.png}
\caption{\label{main-interface-dimension}Taken in version 0.3.0}
\end{figure}
\item Use the dropdown box at the top to select what kind of unit to convert (length, mass, speed, etc.)
\item Select the unit to convert \emph{from} on the left.
\item Select the unit to convert \emph{to} on the right.
\item Enter the value to convert in the box above the convert button.  The program should look something like in figure \ref{sample-conversion-dimension}:
\begin{figure}[htbp]
\centering
\includegraphics[height=250px]{../screenshots/sample-conversion-dimension-converter.png}
\caption{\label{sample-conversion-dimension}This image, taken in version 0.3.0, shows the user about to convert 35 miles to kilometres.}
\end{figure}
\item Press the "Convert" button.  The result will be shown below the "Convert" button.  This is shown in figure \ref{sample-results-dimension}
\begin{figure}[htbp]
\centering
\includegraphics[height=250px]{../screenshots/sample-conversion-results-dimension-converter.png}
\caption{\label{sample-results-dimension}The result of the above conversion}
\end{figure}
\end{enumerate}
\subsection{Complex Unit Conversion}
\label{sec:org75a0192}
\begin{enumerate}
\item Select the "Convert Unit Expressions" if it is not already selected.  You should see a screen like in figure \ref{main-interface-expression}:
\begin{figure}[htbp]
\centering
\includegraphics[height=250px]{../screenshots/main-interface-expression-converter.png}
\caption{\label{main-interface-expression}Taken in version 0.3.0}
\end{figure}
\item Enter a \hyperref[sec:org3724d84]{unit expression} in the From box.  This can be something like "\texttt{7 km}" or "\texttt{6 ft - 2 in}" or "\texttt{3 kg m + 9 lb ft + (35 mm)\textasciicircum{}2 * (85 oz) / (20 in)}".
\item Enter a unit name (or another unit expression) in the To box.
\item Press the Convert button.  This will calculate the value of the first expression, and convert it to a multiple of the second unit (or expression).
\begin{figure}[htbp]
\centering
\includegraphics[height=250px]{../screenshots/sample-conversion-results-expression-converter.png}
\caption{\label{sample-results-expression}A sample calculation.  Divides \texttt{100 km} by \texttt{35 km/h} and converts the result to minutes.  This could be used to calculate how long (in minutes) it takes to go 100 kilometres at a speed of 35 km/h.}
\end{figure}
\end{enumerate}
\section{7Units Settings}
\label{sec:orgae2806f}
All settings can be accessed in the tab with the gear icon.
\begin{figure}[htbp]
\centering
\includegraphics[height=250px]{../screenshots/main-interface-settings.png}
\caption{The settings menu, as of version 0.4.0}
\end{figure}
\subsection{Rounding Settings}
\label{sec:org6d3e49c}
These settings control how the output of a unit conversion is rounded.
\begin{description}
\item[{Fixed Precision}] Round to a fixed number of \href{https://en.wikipedia.org/wiki/Significant\_figures}{significant digits}.  The number of significant digits is controlled by the precision slider below.
\item[{Fixed Decimal Places}] Round to a fixed number of digits after the decimal point.  The number of decimal places is also controlled by the precision slider below.
\item[{Scientific Precision}] Intelligent rounding which uses the precision of the input value(s) to determine the output precision.  Not affected by the precision slider.
\end{description}
\subsection{Prefix Repetition Settings}
\label{sec:org9aa98f8}
These settings control when you are allowed to repeat unit prefixes (e.g. kilokilometre)
\begin{description}
\item[{No Repetition}] Units may only have one prefix.
\item[{No Restriction}] Units may have any number of prefixes.
\item[{Complex Repetition}] A complex rule which makes it so that each power of 10 has one and only one prefix combination.  Units may have the following prefixes:
\begin{itemize}
\item one of: centi, deci, deca, hecto
\item one of: zepto, atto, femto, pico, nano, micro, milli, kilo, mega, giga, tera, peta, exa, zetta
\item any number of yocto or yotta
\item they must be in this order
\item all prefixes must be of the same sign (either all magnifying or all reducing)
\end{itemize}
\end{description}
\subsection{Search Settings}
\label{sec:org2745ba0}
These settings control which prefixes are shown in the "Convert Units" tab.  Only coherent SI units (e.g. metre, second, newton, joule) will get prefixes.  Some prefixed units are created in the unitfile, and will stay regardless of this setting (though they can be removed from the unitfile).
\begin{description}
\item[{Never Include Prefixed Units}] Prefixed units will only be shown if they are explicitly added to the unitfile.
\item[{Include Common Prefixes}] Every coherent unit will have its kilo- and milli- versions included in the list.
\item[{Include All Single Prefixes}] Every coherent unit will have every prefixed version of it included in the list.
\end{description}
\subsection{Miscellaneous Settings}
\label{sec:orgeabb2df}
\begin{description}
\item[{Convert One Way Only}] In the simple conversion tab, only imperial/customary units will be shown on the left, and only metric units\footnote{7Units's definition of "metric" is stricter than the SI, but all of the common units that are commonly considered metric but not included in 7Units's definition are included in the exceptions file.} will be shown on the right.  Units listed in the exceptions file (\texttt{src/main/resources/metric\_exceptions.txt}) will be shown on both sides.  This is a way to reduce the number of options you must search through if you only convert one way.  The expressions tab is unaffected.
\item[{Show Duplicates in "Convert Units"}] If unchecked, any unit that has multiple names will only have one included in the Convert Units lists.  The selected name will be the longest; if there are multiple longest names one is selected arbitrarily.  You will still be able to use these alternate names in the expressions tab.
\end{description}
\subsection{Configuration File}
\label{sec:org4cc2874}
The settings are saved in a configuration file.  On Windows, this is located at \\[0pt]
\texttt{\%USERPROFILE\%/AppData/Local/SevenUnits/config.txt}.  On other operating systems, this is located at \texttt{\$HOME/.config/SevenUnits/config.txt}.  The directory containing the \texttt{SevenUnits} directory can be overridden with the environment variables \texttt{\$LOCALAPPDATA} on Windows or \texttt{\$XDG\_CONFIG\_HOME} elsewhere.

Each line of this file contains a setting name, followed by an equal sign (not surrounded by spaces), then the setting value.

The possible setting names are:
\begin{description}
\item[{\texttt{number\_display\_rule}}] The rounding rule.  This can be \texttt{FIXED\_DECIMALS}, \texttt{FIXED\_PRECISION}, or \texttt{FIXED\_UNCERTAINTY}.  The first two must have a space then a number after them to set the number of decimal places or significant digits.
\item[{\texttt{prefix\_rule}}] The prefix repetition rule; Can be either \texttt{NO\_REPETITION}, \texttt{NO\_RESTRICTION}, \\[0pt]
or \texttt{COMPLEX\_REPETITION}.
\item[{\texttt{one\_way}}] Whether One-Way Conversion is enabled; can be either \texttt{true} or \texttt{false}.
\item[{\texttt{include\_duplicates}}] Whether duplicate units should be shown; can be either \texttt{true} or \texttt{false}.
\item[{\texttt{search\_prefix\_rule}}] The prefix search rule; can be \texttt{NO\_PREFIXES}, \texttt{COMMON\_PREFIXES}, \\[0pt]
or \texttt{ALL\_METRIC\_PREFIXES}.
\end{description}

You can also use the special setting names \texttt{custom\_unit\_file}, \texttt{custom\_dimension\_file} and \texttt{custom\_exception\_file} to add custom units, dimensions and metric exceptions to the system.  These files use the same format as the standard files.  These setting names can be used more than once to include multiple unit, dimension or exception files.
\section{Appendices}
\label{sec:org60385a7}
\subsection{Unit Expressions}
\label{sec:org3724d84}
A unit expression is simply a math expression where the values being operated on are units or numbers.  The operations that can be used are (in order of precedence):
\begin{itemize}
\item Exponentiation (\^{}); the exponent must be an integer.  Both units and numbers can be raised to an exponent
\item Multiplication (*) and division (/).  Multiplication can also be done with a space (so "15 meter" is the same thing as "15 * meter").
You can also divide with \texttt{|} to create fractions.  Using \texttt{|} instead of \texttt{/} gives the division a higher precedence than any other operator.  For example, "2|5\textsuperscript{2}" evaluates to 4/25, not 2/25.
\item Addition (+) and subtraction (-).  They can only be done between units of the same dimension (measuring the same thing).  So you can add metres, inches and feet together, and you can add joules and calories together, but you can't add metres to seconds, or feet to calories, or watts to pounds.
\end{itemize}

Brackets can be used to manipulate the order of operations, and nonlinear units like Celsius and Fahrenheit cannot be used in expressions.  You can use a value in a nonlinear unit by putting brackets after it - for example, degC(12) represents the value 12 \textdegree{} C
\subsection{Other Expressions}
\label{sec:orgc72a672}
There are also a simplified version of expressions for prefixes and dimensions.  Only multiplication, division and exponentation are supported.  Currently, exponentation is not supported for dimensions, but that may be fixed in the future.
\end{document}
