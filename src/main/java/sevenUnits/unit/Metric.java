/**
 * Copyright (C) 2018 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnits.unit;

import java.util.Set;

import sevenUnits.utils.NameSymbol;
import sevenUnits.utils.ObjectProduct;

/**
 * All of the units, prefixes and dimensions that are used by the SI, as well as
 * some outside the SI.
 * 
 * <p>
 * This class does not include prefixed units. To obtain prefixed units, use
 * {@link LinearUnit#withPrefix}:
 * 
 * <pre>
 * LinearUnit KILOMETRE = SI.METRE.withPrefix(SI.KILO);
 * </pre>
 * 
 * 
 * @author Adrien Hopkins
 * @since 2019-10-16
 */
public final class Metric {
	/// dimensions used by SI units
	// base dimensions, as BaseDimensions
	public static final class BaseDimensions {
		public static final BaseDimension LENGTH = BaseDimension.valueOf("Length",
				"L");
		public static final BaseDimension MASS = BaseDimension.valueOf("Mass",
				"M");
		public static final BaseDimension TIME = BaseDimension.valueOf("Time",
				"T");
		public static final BaseDimension ELECTRIC_CURRENT = BaseDimension
				.valueOf("Electric Current", "I");
		public static final BaseDimension TEMPERATURE = BaseDimension
				.valueOf("Temperature", "\u0398"); // theta symbol
		public static final BaseDimension QUANTITY = BaseDimension
				.valueOf("Quantity", "N");
		public static final BaseDimension LUMINOUS_INTENSITY = BaseDimension
				.valueOf("Luminous Intensity", "J");
		public static final BaseDimension INFORMATION = BaseDimension
				.valueOf("Information", "Info"); // non-SI
		public static final BaseDimension CURRENCY = BaseDimension
				.valueOf("Currency", "$$"); // non-SI

		// You may NOT get SI.BaseDimensions instances!
		private BaseDimensions() {
			throw new AssertionError();
		}
	}

	/// base units of the SI
	// suppressing warnings since these are the same object, but in a different
	/// form (class)
	@SuppressWarnings("hiding")
	public static final class BaseUnits {
		public static final BaseUnit METRE = BaseUnit
				.valueOf(BaseDimensions.LENGTH, "metre", "m");
		public static final BaseUnit KILOGRAM = BaseUnit
				.valueOf(BaseDimensions.MASS, "kilogram", "kg");
		public static final BaseUnit SECOND = BaseUnit
				.valueOf(BaseDimensions.TIME, "second", "s");
		public static final BaseUnit AMPERE = BaseUnit
				.valueOf(BaseDimensions.ELECTRIC_CURRENT, "ampere", "A");
		public static final BaseUnit KELVIN = BaseUnit
				.valueOf(BaseDimensions.TEMPERATURE, "kelvin", "K");
		public static final BaseUnit MOLE = BaseUnit
				.valueOf(BaseDimensions.QUANTITY, "mole", "mol");
		public static final BaseUnit CANDELA = BaseUnit
				.valueOf(BaseDimensions.LUMINOUS_INTENSITY, "candela", "cd");
		public static final BaseUnit BIT = BaseUnit
				.valueOf(BaseDimensions.INFORMATION, "bit", "b");
		public static final BaseUnit DOLLAR = BaseUnit
				.valueOf(BaseDimensions.CURRENCY, "dollar", "$");

		public static final Set<BaseUnit> BASE_UNITS = Set.of(METRE, KILOGRAM,
				SECOND, AMPERE, KELVIN, MOLE, CANDELA, BIT);

		// You may NOT get SI.BaseUnits instances!
		private BaseUnits() {
			throw new AssertionError();
		}
	}

	/**
	 * Constants that relate to the SI or other systems.
	 * 
	 * @author Adrien Hopkins
	 * @since 2019-11-08
	 */
	public static final class Constants {
		public static final LinearUnit EARTH_GRAVITY = METRE.dividedBy(SECOND)
				.dividedBy(SECOND).times(9.80665);
	}

	// dimensions used in the SI, as ObjectProducts
	public static final class Dimensions {
		public static final ObjectProduct<BaseDimension> EMPTY = ObjectProduct
				.empty();
		public static final ObjectProduct<BaseDimension> LENGTH = ObjectProduct
				.oneOf(BaseDimensions.LENGTH)
				.withName(NameSymbol.of("Length", "L"));
		public static final ObjectProduct<BaseDimension> MASS = ObjectProduct
				.oneOf(BaseDimensions.MASS).withName(NameSymbol.of("Mass", "M"));
		public static final ObjectProduct<BaseDimension> TIME = ObjectProduct
				.oneOf(BaseDimensions.TIME).withName(NameSymbol.of("Time", "T"));
		public static final ObjectProduct<BaseDimension> ELECTRIC_CURRENT = ObjectProduct
				.oneOf(BaseDimensions.ELECTRIC_CURRENT)
				.withName(NameSymbol.of("Current", "I"));
		public static final ObjectProduct<BaseDimension> TEMPERATURE = ObjectProduct
				.oneOf(BaseDimensions.TEMPERATURE)
				.withName(NameSymbol.of("Temperature", "\u0398"));
		public static final ObjectProduct<BaseDimension> QUANTITY = ObjectProduct
				.oneOf(BaseDimensions.QUANTITY)
				.withName(NameSymbol.of("Quantity", "N"));
		public static final ObjectProduct<BaseDimension> LUMINOUS_INTENSITY = ObjectProduct
				.oneOf(BaseDimensions.LUMINOUS_INTENSITY)
				.withName(NameSymbol.of("Luminous Intensity", "J"));
		public static final ObjectProduct<BaseDimension> INFORMATION = ObjectProduct
				.oneOf(BaseDimensions.INFORMATION)
				.withName(NameSymbol.ofName("Information"));
		public static final ObjectProduct<BaseDimension> CURRENCY = ObjectProduct
				.oneOf(BaseDimensions.CURRENCY)
				.withName(NameSymbol.ofName("Currency"));

		// derived dimensions without named SI units
		public static final ObjectProduct<BaseDimension> AREA = LENGTH
				.times(LENGTH);
		public static final ObjectProduct<BaseDimension> VOLUME = AREA
				.times(LENGTH);
		public static final ObjectProduct<BaseDimension> VELOCITY = LENGTH
				.dividedBy(TIME).withName(NameSymbol.ofName("Velocity"));
		public static final ObjectProduct<BaseDimension> ACCELERATION = VELOCITY
				.dividedBy(TIME);
		public static final ObjectProduct<BaseDimension> WAVENUMBER = EMPTY
				.dividedBy(LENGTH);
		public static final ObjectProduct<BaseDimension> MASS_DENSITY = MASS
				.dividedBy(VOLUME);
		public static final ObjectProduct<BaseDimension> SURFACE_DENSITY = MASS
				.dividedBy(AREA);
		public static final ObjectProduct<BaseDimension> SPECIFIC_VOLUME = VOLUME
				.dividedBy(MASS);
		public static final ObjectProduct<BaseDimension> CURRENT_DENSITY = ELECTRIC_CURRENT
				.dividedBy(AREA);
		public static final ObjectProduct<BaseDimension> MAGNETIC_FIELD_STRENGTH = ELECTRIC_CURRENT
				.dividedBy(LENGTH);
		public static final ObjectProduct<BaseDimension> CONCENTRATION = QUANTITY
				.dividedBy(VOLUME);
		public static final ObjectProduct<BaseDimension> MASS_CONCENTRATION = CONCENTRATION
				.times(MASS);
		public static final ObjectProduct<BaseDimension> LUMINANCE = LUMINOUS_INTENSITY
				.dividedBy(AREA);
		public static final ObjectProduct<BaseDimension> REFRACTIVE_INDEX = VELOCITY
				.dividedBy(VELOCITY);
		public static final ObjectProduct<BaseDimension> REFRACTIVE_PERMEABILITY = EMPTY
				.times(EMPTY);
		public static final ObjectProduct<BaseDimension> ANGLE = LENGTH
				.dividedBy(LENGTH);
		public static final ObjectProduct<BaseDimension> SOLID_ANGLE = AREA
				.dividedBy(AREA);

		// derived dimensions with named SI units
		public static final ObjectProduct<BaseDimension> FREQUENCY = EMPTY
				.dividedBy(TIME);
		public static final ObjectProduct<BaseDimension> FORCE = MASS
				.times(ACCELERATION);
		public static final ObjectProduct<BaseDimension> ENERGY = FORCE
				.times(LENGTH);
		public static final ObjectProduct<BaseDimension> POWER = ENERGY
				.dividedBy(TIME);
		public static final ObjectProduct<BaseDimension> ELECTRIC_CHARGE = ELECTRIC_CURRENT
				.times(TIME);
		public static final ObjectProduct<BaseDimension> VOLTAGE = ENERGY
				.dividedBy(ELECTRIC_CHARGE);
		public static final ObjectProduct<BaseDimension> CAPACITANCE = ELECTRIC_CHARGE
				.dividedBy(VOLTAGE);
		public static final ObjectProduct<BaseDimension> ELECTRIC_RESISTANCE = VOLTAGE
				.dividedBy(ELECTRIC_CURRENT);
		public static final ObjectProduct<BaseDimension> ELECTRIC_CONDUCTANCE = ELECTRIC_CURRENT
				.dividedBy(VOLTAGE);
		public static final ObjectProduct<BaseDimension> MAGNETIC_FLUX = VOLTAGE
				.times(TIME);
		public static final ObjectProduct<BaseDimension> MAGNETIC_FLUX_DENSITY = MAGNETIC_FLUX
				.dividedBy(AREA);
		public static final ObjectProduct<BaseDimension> INDUCTANCE = MAGNETIC_FLUX
				.dividedBy(ELECTRIC_CURRENT);
		public static final ObjectProduct<BaseDimension> LUMINOUS_FLUX = LUMINOUS_INTENSITY
				.times(SOLID_ANGLE);
		public static final ObjectProduct<BaseDimension> ILLUMINANCE = LUMINOUS_FLUX
				.dividedBy(AREA);
		public static final ObjectProduct<BaseDimension> SPECIFIC_ENERGY = ENERGY
				.dividedBy(MASS);
		public static final ObjectProduct<BaseDimension> CATALYTIC_ACTIVITY = QUANTITY
				.dividedBy(TIME);

		// You may NOT get SI.Dimension instances!
		private Dimensions() {
			throw new AssertionError();
		}
	}

	/// The units of the SI
	public static final LinearUnit ONE = LinearUnit
			.valueOf(ObjectProduct.empty(), 1);

	public static final LinearUnit METRE = BaseUnits.METRE.asLinearUnit()
			.withName(NameSymbol.of("metre", "m", "meter"));
	public static final LinearUnit KILOGRAM = BaseUnits.KILOGRAM.asLinearUnit()
			.withName(NameSymbol.of("kilogram", "kg"));
	public static final LinearUnit SECOND = BaseUnits.SECOND.asLinearUnit()
			.withName(NameSymbol.of("second", "s", "sec"));
	public static final LinearUnit AMPERE = BaseUnits.AMPERE.asLinearUnit()
			.withName(NameSymbol.of("ampere", "A"));
	public static final LinearUnit KELVIN = BaseUnits.KELVIN.asLinearUnit()
			.withName(NameSymbol.of("kelvin", "K"));
	public static final LinearUnit MOLE = BaseUnits.MOLE.asLinearUnit()
			.withName(NameSymbol.of("mole", "mol"));
	public static final LinearUnit CANDELA = BaseUnits.CANDELA.asLinearUnit()
			.withName(NameSymbol.of("candela", "cd"));
	public static final LinearUnit BIT = BaseUnits.BIT.asLinearUnit()
			.withName(NameSymbol.of("bit", "b"));
	public static final LinearUnit DOLLAR = BaseUnits.DOLLAR.asLinearUnit()
			.withName(NameSymbol.of("dollar", "$"));
	// Non-base units
	public static final LinearUnit RADIAN = METRE.dividedBy(METRE)
			.withName(NameSymbol.of("radian", "rad"));

	public static final LinearUnit STERADIAN = RADIAN.times(RADIAN)
			.withName(NameSymbol.of("steradian", "sr"));
	public static final LinearUnit HERTZ = ONE.dividedBy(SECOND)
			.withName(NameSymbol.of("hertz", "Hz"));
	// for periodic phenomena
	public static final LinearUnit NEWTON = KILOGRAM.times(METRE)
			.dividedBy(SECOND.times(SECOND))
			.withName(NameSymbol.of("newton", "N"));
	public static final LinearUnit PASCAL = NEWTON.dividedBy(METRE.times(METRE))
			.withName(NameSymbol.of("pascal", "Pa"));
	public static final LinearUnit JOULE = NEWTON.times(METRE)
			.withName(NameSymbol.of("joule", "J"));
	public static final LinearUnit WATT = JOULE.dividedBy(SECOND)
			.withName(NameSymbol.of("watt", "W"));
	public static final LinearUnit COULOMB = AMPERE.times(SECOND)
			.withName(NameSymbol.of("coulomb", "C"));
	public static final LinearUnit VOLT = JOULE.dividedBy(COULOMB)
			.withName(NameSymbol.of("volt", "V"));
	public static final LinearUnit FARAD = COULOMB.dividedBy(VOLT)
			.withName(NameSymbol.of("farad", "F"));
	public static final LinearUnit OHM = VOLT.dividedBy(AMPERE)
			.withName(NameSymbol.of("ohm", "\u03A9")); // omega
	public static final LinearUnit SIEMENS = ONE.dividedBy(OHM)
			.withName(NameSymbol.of("siemens", "S"));
	public static final LinearUnit WEBER = VOLT.times(SECOND)
			.withName(NameSymbol.of("weber", "Wb"));
	public static final LinearUnit TESLA = WEBER.dividedBy(METRE.times(METRE))
			.withName(NameSymbol.of("tesla", "T"));
	public static final LinearUnit HENRY = WEBER.dividedBy(AMPERE)
			.withName(NameSymbol.of("henry", "H"));
	public static final LinearUnit LUMEN = CANDELA.times(STERADIAN)
			.withName(NameSymbol.of("lumen", "lm"));
	public static final LinearUnit LUX = LUMEN.dividedBy(METRE.times(METRE))
			.withName(NameSymbol.of("lux", "lx"));
	public static final LinearUnit BEQUEREL = ONE.dividedBy(SECOND)
			.withName(NameSymbol.of("bequerel", "Bq"));
	// for activity referred to a nucleotide
	public static final LinearUnit GRAY = JOULE.dividedBy(KILOGRAM)
			.withName(NameSymbol.of("grey", "Gy"));
	// for absorbed dose
	public static final LinearUnit SIEVERT = JOULE.dividedBy(KILOGRAM)
			.withName(NameSymbol.of("sievert", "Sv"));
	// for dose equivalent
	public static final LinearUnit KATAL = MOLE.dividedBy(SECOND)
			.withName(NameSymbol.of("katal", "kat"));
	// common derived units included for convenience
	public static final LinearUnit GRAM = KILOGRAM.dividedBy(1000)
			.withName(NameSymbol.of("gram", "g"));

	public static final LinearUnit SQUARE_METRE = METRE.toExponent(2)
			.withName(NameSymbol.of("square metre", "m^2", "square meter",
					"metre squared", "meter squared"));
	public static final LinearUnit CUBIC_METRE = METRE.toExponent(3)
			.withName(NameSymbol.of("cubic metre", "m^3", "cubic meter",
					"metre cubed", "meter cubed"));
	public static final LinearUnit METRE_PER_SECOND = METRE.dividedBy(SECOND)
			.withName(
					NameSymbol.of("metre per second", "m/s", "meter per second"));
	// Non-SI units included for convenience
	public static final Unit CELSIUS = Unit
			.fromConversionFunctions(KELVIN.getBase(), tempK -> tempK - 273.15,
					tempC -> tempC + 273.15)
			.withName(NameSymbol.of("degree Celsius", "\u00B0C"));

	public static final LinearUnit MINUTE = SECOND.times(60)
			.withName(NameSymbol.of("minute", "min"));
	public static final LinearUnit HOUR = MINUTE.times(60)
			.withName(NameSymbol.of("hour", "h", "hr"));
	public static final LinearUnit DAY = HOUR.times(60)
			.withName(NameSymbol.of("day", "d"));
	public static final LinearUnit KILOMETRE_PER_HOUR = METRE.times(1000)
			.dividedBy(HOUR).withName(NameSymbol.of("kilometre per hour", "km/h",
					"kilometer per hour"));
	public static final LinearUnit DEGREE = RADIAN.times(360 / (2 * Math.PI))
			.withName(NameSymbol.of("degree", "\u00B0", "deg"));
	public static final LinearUnit ARCMINUTE = DEGREE.dividedBy(60)
			.withName(NameSymbol.of("arcminute", "arcmin"));
	public static final LinearUnit ARCSECOND = ARCMINUTE.dividedBy(60)
			.withName(NameSymbol.of("arcsecond", "arcsec"));
	public static final LinearUnit ASTRONOMICAL_UNIT = METRE
			.times(149597870700.0)
			.withName(NameSymbol.of("astronomical unit", "au"));
	public static final LinearUnit PARSEC = ASTRONOMICAL_UNIT
			.dividedBy(ARCSECOND).withName(NameSymbol.of("parsec", "pc"));
	public static final LinearUnit HECTARE = METRE.times(METRE).times(10000.0)
			.withName(NameSymbol.of("hectare", "ha"));
	public static final LinearUnit LITRE = METRE.times(METRE).times(METRE)
			.dividedBy(1000.0).withName(NameSymbol.of("litre", "L", "l", "liter"));
	public static final LinearUnit TONNE = KILOGRAM.times(1000.0)
			.withName(NameSymbol.of("tonne", "t", "metric ton"));
	public static final LinearUnit DALTON = KILOGRAM.times(1.660539040e-27)
			.withName(NameSymbol.of("dalton", "Da", "atomic unit", "u")); // approximate
	// value
	public static final LinearUnit ELECTRONVOLT = JOULE.times(1.602176634e-19)
			.withName(NameSymbol.of("electron volt", "eV"));
	public static final LinearUnit BYTE = BIT.times(8)
			.withName(NameSymbol.of("byte", "B"));
	public static final Unit NEPER = Unit.fromConversionFunctions(ONE.getBase(),
			pr -> 0.5 * Math.log(pr), Np -> Math.exp(2 * Np))
			.withName(NameSymbol.of("neper", "Np"));
	public static final Unit BEL = Unit.fromConversionFunctions(ONE.getBase(),
			pr -> Math.log10(pr), dB -> Math.pow(10, dB))
			.withName(NameSymbol.of("bel", "B"));
	public static final Unit DECIBEL = Unit
			.fromConversionFunctions(ONE.getBase(), pr -> 10 * Math.log10(pr),
					dB -> Math.pow(10, dB / 10))
			.withName(NameSymbol.of("decibel", "dB"));

	/// The prefixes of the SI
	// expanding decimal prefixes
	public static final UnitPrefix KILO = UnitPrefix.valueOf(1e3)
			.withName(NameSymbol.of("kilo", "k", "K"));
	public static final UnitPrefix MEGA = UnitPrefix.valueOf(1e6)
			.withName(NameSymbol.of("mega", "M"));
	public static final UnitPrefix GIGA = UnitPrefix.valueOf(1e9)
			.withName(NameSymbol.of("giga", "G"));
	public static final UnitPrefix TERA = UnitPrefix.valueOf(1e12)
			.withName(NameSymbol.of("tera", "T"));
	public static final UnitPrefix PETA = UnitPrefix.valueOf(1e15)
			.withName(NameSymbol.of("peta", "P"));
	public static final UnitPrefix EXA = UnitPrefix.valueOf(1e18)
			.withName(NameSymbol.of("exa", "E"));
	public static final UnitPrefix ZETTA = UnitPrefix.valueOf(1e21)
			.withName(NameSymbol.of("zetta", "Z"));
	public static final UnitPrefix YOTTA = UnitPrefix.valueOf(1e24)
			.withName(NameSymbol.of("yotta", "Y"));

	// contracting decimal prefixes
	public static final UnitPrefix MILLI = UnitPrefix.valueOf(1e-3)
			.withName(NameSymbol.of("milli", "m"));
	public static final UnitPrefix MICRO = UnitPrefix.valueOf(1e-6)
			.withName(NameSymbol.of("micro", "\u03BC", "u")); // mu
	public static final UnitPrefix NANO = UnitPrefix.valueOf(1e-9)
			.withName(NameSymbol.of("nano", "n"));
	public static final UnitPrefix PICO = UnitPrefix.valueOf(1e-12)
			.withName(NameSymbol.of("pico", "p"));
	public static final UnitPrefix FEMTO = UnitPrefix.valueOf(1e-15)
			.withName(NameSymbol.of("femto", "f"));
	public static final UnitPrefix ATTO = UnitPrefix.valueOf(1e-18)
			.withName(NameSymbol.of("atto", "a"));
	public static final UnitPrefix ZEPTO = UnitPrefix.valueOf(1e-21)
			.withName(NameSymbol.of("zepto", "z"));
	public static final UnitPrefix YOCTO = UnitPrefix.valueOf(1e-24)
			.withName(NameSymbol.of("yocto", "y"));

	// prefixes that don't match the pattern of thousands
	public static final UnitPrefix DEKA = UnitPrefix.valueOf(1e1)
			.withName(NameSymbol.of("deka", "da", "deca", "D"));
	public static final UnitPrefix HECTO = UnitPrefix.valueOf(1e2)
			.withName(NameSymbol.of("hecto", "h", "H", "hekto"));
	public static final UnitPrefix DECI = UnitPrefix.valueOf(1e-1)
			.withName(NameSymbol.of("deci", "d"));
	public static final UnitPrefix CENTI = UnitPrefix.valueOf(1e-2)
			.withName(NameSymbol.of("centi", "c"));
	public static final UnitPrefix KIBI = UnitPrefix.valueOf(1024)
			.withName(NameSymbol.of("kibi", "Ki"));
	public static final UnitPrefix MEBI = KIBI.times(1024)
			.withName(NameSymbol.of("mebi", "Mi"));
	public static final UnitPrefix GIBI = MEBI.times(1024)
			.withName(NameSymbol.of("gibi", "Gi"));
	public static final UnitPrefix TEBI = GIBI.times(1024)
			.withName(NameSymbol.of("tebi", "Ti"));
	public static final UnitPrefix PEBI = TEBI.times(1024)
			.withName(NameSymbol.of("pebi", "Pi"));
	public static final UnitPrefix EXBI = PEBI.times(1024)
			.withName(NameSymbol.of("exbi", "Ei"));

	// a few prefixed units
	public static final LinearUnit MICROMETRE = Metric.METRE
			.withPrefix(Metric.MICRO);
	public static final LinearUnit MILLIMETRE = Metric.METRE
			.withPrefix(Metric.MILLI);
	public static final LinearUnit KILOMETRE = Metric.METRE
			.withPrefix(Metric.KILO);
	public static final LinearUnit MEGAMETRE = Metric.METRE
			.withPrefix(Metric.MEGA);

	public static final LinearUnit MICROLITRE = Metric.LITRE
			.withPrefix(Metric.MICRO);
	public static final LinearUnit MILLILITRE = Metric.LITRE
			.withPrefix(Metric.MILLI);
	public static final LinearUnit KILOLITRE = Metric.LITRE
			.withPrefix(Metric.KILO);
	public static final LinearUnit MEGALITRE = Metric.LITRE
			.withPrefix(Metric.MEGA);

	public static final LinearUnit MICROSECOND = Metric.SECOND
			.withPrefix(Metric.MICRO);
	public static final LinearUnit MILLISECOND = Metric.SECOND
			.withPrefix(Metric.MILLI);
	public static final LinearUnit KILOSECOND = Metric.SECOND
			.withPrefix(Metric.KILO);
	public static final LinearUnit MEGASECOND = Metric.SECOND
			.withPrefix(Metric.MEGA);

	public static final LinearUnit MICROGRAM = Metric.GRAM
			.withPrefix(Metric.MICRO);
	public static final LinearUnit MILLIGRAM = Metric.GRAM
			.withPrefix(Metric.MILLI);
	public static final LinearUnit MEGAGRAM = Metric.GRAM
			.withPrefix(Metric.MEGA);

	public static final LinearUnit MICRONEWTON = Metric.NEWTON
			.withPrefix(Metric.MICRO);
	public static final LinearUnit MILLINEWTON = Metric.NEWTON
			.withPrefix(Metric.MILLI);
	public static final LinearUnit KILONEWTON = Metric.NEWTON
			.withPrefix(Metric.KILO);
	public static final LinearUnit MEGANEWTON = Metric.NEWTON
			.withPrefix(Metric.MEGA);

	public static final LinearUnit MICROJOULE = Metric.JOULE
			.withPrefix(Metric.MICRO);
	public static final LinearUnit MILLIJOULE = Metric.JOULE
			.withPrefix(Metric.MILLI);
	public static final LinearUnit KILOJOULE = Metric.JOULE
			.withPrefix(Metric.KILO);
	public static final LinearUnit MEGAJOULE = Metric.JOULE
			.withPrefix(Metric.MEGA);

	public static final LinearUnit MICROWATT = Metric.WATT
			.withPrefix(Metric.MICRO);
	public static final LinearUnit MILLIWATT = Metric.WATT
			.withPrefix(Metric.MILLI);
	public static final LinearUnit KILOWATT = Metric.WATT
			.withPrefix(Metric.KILO);
	public static final LinearUnit MEGAWATT = Metric.WATT
			.withPrefix(Metric.MEGA);

	public static final LinearUnit MICROCOULOMB = Metric.COULOMB
			.withPrefix(Metric.MICRO);
	public static final LinearUnit MILLICOULOMB = Metric.COULOMB
			.withPrefix(Metric.MILLI);
	public static final LinearUnit KILOCOULOMB = Metric.COULOMB
			.withPrefix(Metric.KILO);
	public static final LinearUnit MEGACOULOMB = Metric.COULOMB
			.withPrefix(Metric.MEGA);

	public static final LinearUnit MICROAMPERE = Metric.AMPERE
			.withPrefix(Metric.MICRO);
	public static final LinearUnit MILLIAMPERE = Metric.AMPERE
			.withPrefix(Metric.MILLI);

	public static final LinearUnit MICROVOLT = Metric.VOLT
			.withPrefix(Metric.MICRO);
	public static final LinearUnit MILLIVOLT = Metric.VOLT
			.withPrefix(Metric.MILLI);
	public static final LinearUnit KILOVOLT = Metric.VOLT
			.withPrefix(Metric.KILO);
	public static final LinearUnit MEGAVOLT = Metric.VOLT
			.withPrefix(Metric.MEGA);

	public static final LinearUnit KILOOHM = Metric.OHM.withPrefix(Metric.KILO);
	public static final LinearUnit MEGAOHM = Metric.OHM.withPrefix(Metric.MEGA);

	// sets of prefixes
	public static final Set<UnitPrefix> ALL_PREFIXES = Set.of(DEKA, HECTO, KILO,
			MEGA, GIGA, TERA, PETA, EXA, ZETTA, YOTTA, DECI, CENTI, MILLI, MICRO,
			NANO, PICO, FEMTO, ATTO, ZEPTO, YOCTO, KIBI, MEBI, GIBI, TEBI, PEBI,
			EXBI);

	public static final Set<UnitPrefix> DECIMAL_PREFIXES = Set.of(DEKA, HECTO,
			KILO, MEGA, GIGA, TERA, PETA, EXA, ZETTA, YOTTA, DECI, CENTI, MILLI,
			MICRO, NANO, PICO, FEMTO, ATTO, ZEPTO, YOCTO);
	public static final Set<UnitPrefix> THOUSAND_PREFIXES = Set.of(KILO, MEGA,
			GIGA, TERA, PETA, EXA, ZETTA, YOTTA, MILLI, MICRO, NANO, PICO, FEMTO,
			ATTO, ZEPTO, YOCTO);
	public static final Set<UnitPrefix> MAGNIFYING_PREFIXES = Set.of(DEKA, HECTO,
			KILO, MEGA, GIGA, TERA, PETA, EXA, ZETTA, YOTTA, KIBI, MEBI, GIBI,
			TEBI, PEBI, EXBI);
	public static final Set<UnitPrefix> REDUCING_PREFIXES = Set.of(DECI, CENTI,
			MILLI, MICRO, NANO, PICO, FEMTO, ATTO, ZEPTO, YOCTO);

	// You may NOT get SI instances!
	private Metric() {
		throw new AssertionError();
	}
}
