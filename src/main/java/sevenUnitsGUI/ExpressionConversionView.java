/**
 * Copyright (C) 2021 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnitsGUI;

/**
 * A View that can convert unit expressions
 * 
 * @author Adrien Hopkins
 * @since v0.4.0
 * @since 2021-12-15
 */
public interface ExpressionConversionView extends View {
	/**
	 * @return unit expression to convert <em>from</em>
	 * @since v0.4.0
	 * @since 2021-12-15
	 */
	String getFromExpression();

	/**
	 * @return unit expression to convert <em>to</em>
	 * @since v0.4.0
	 * @since 2021-12-15
	 */
	String getToExpression();

	/**
	 * Shows the output of an expression conversion to the user.
	 * 
	 * @param uc unit conversion to show
	 * @since v0.4.0
	 * @since 2021-12-15
	 */
	void showExpressionConversionOutput(UnitConversionRecord uc);
}
