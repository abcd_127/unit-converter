/**
 * Copyright (C) 2019 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnits.unit;

import java.util.Objects;
import java.util.function.DoubleUnaryOperator;

import sevenUnits.utils.NameSymbol;
import sevenUnits.utils.ObjectProduct;

/**
 * A unit that uses functional objects to convert to and from its base.
 * 
 * @author Adrien Hopkins
 * @since 2019-05-22
 */
final class FunctionalUnit extends Unit {
	/**
	 * A function that accepts a value expressed in the unit's base and returns
	 * that value expressed in this unit.
	 * 
	 * @since 2019-05-22
	 */
	private final DoubleUnaryOperator converterFrom;

	/**
	 * A function that accepts a value expressed in the unit and returns that
	 * value expressed in the unit's base.
	 * 
	 * @since 2019-05-22
	 */
	private final DoubleUnaryOperator converterTo;

	/**
	 * Creates the {@code FunctionalUnit}.
	 * 
	 * @param base          unit's base
	 * @param converterFrom function that accepts a value expressed in the unit's
	 *                      base and returns that value expressed in this unit.
	 * @param converterTo   function that accepts a value expressed in the unit
	 *                      and returns that value expressed in the unit's base.
	 * @throws NullPointerException if any argument is null
	 * @since 2019-05-22
	 */
	public FunctionalUnit(final ObjectProduct<BaseUnit> base,
			final DoubleUnaryOperator converterFrom,
			final DoubleUnaryOperator converterTo) {
		super(base, NameSymbol.EMPTY);
		this.converterFrom = Objects.requireNonNull(converterFrom,
				"converterFrom must not be null.");
		this.converterTo = Objects.requireNonNull(converterTo,
				"converterTo must not be null.");
	}

	/**
	 * Creates the {@code FunctionalUnit}.
	 * 
	 * @param base          unit's base
	 * @param converterFrom function that accepts a value expressed in the unit's
	 *                      base and returns that value expressed in this unit.
	 * @param converterTo   function that accepts a value expressed in the unit
	 *                      and returns that value expressed in the unit's base.
	 * @throws NullPointerException if any argument is null
	 * @since 2019-05-22
	 */
	public FunctionalUnit(final ObjectProduct<BaseUnit> base,
			final DoubleUnaryOperator converterFrom,
			final DoubleUnaryOperator converterTo, final NameSymbol ns) {
		super(base, ns);
		this.converterFrom = Objects.requireNonNull(converterFrom,
				"converterFrom must not be null.");
		this.converterTo = Objects.requireNonNull(converterTo,
				"converterTo must not be null.");
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Uses {@code converterFrom} to convert.
	 */
	@Override
	public double convertFromBase(final double value) {
		return this.converterFrom.applyAsDouble(value);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Uses {@code converterTo} to convert.
	 */
	@Override
	public double convertToBase(final double value) {
		return this.converterTo.applyAsDouble(value);
	}

}
