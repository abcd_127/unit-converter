/**
 * Copyright (C) 2021-2022 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnitsGUI;

import java.util.Optional;
import java.util.Set;

/**
 * A View that supports single unit-based conversion
 * 
 * @author Adrien Hopkins
 * @since v0.4.0
 * @since 2021-12-15
 */
public interface UnitConversionView extends View {
	/**
	 * @return dimensions available for filtering
	 * @since v0.4.0
	 * @since 2022-01-29
	 */
	Set<String> getDimensionNames();

	/**
	 * @return name of unit to convert <em>from</em>
	 * @since v0.4.0
	 * @since 2021-12-15
	 */
	Optional<String> getFromSelection();

	/**
	 * @return list of names of units available to convert from
	 * @since v0.4.0
	 * @since 2022-03-30
	 */
	Set<String> getFromUnitNames();

	/**
	 * @return value to convert between the units (specifically, the numeric
	 *         string provided by the user)
	 * @since v0.4.0
	 * @since 2021-12-15
	 */
	String getInputValue();

	/**
	 * @return selected dimension
	 * @since v0.4.0
	 * @since 2021-12-15
	 */
	Optional<String> getSelectedDimensionName();

	/**
	 * @return name of unit to convert <em>to</em>
	 * @since v0.4.0
	 * @since 2021-12-15
	 */
	Optional<String> getToSelection();

	/**
	 * @return list of names of units available to convert to
	 * @since v0.4.0
	 * @since 2022-03-30
	 */
	Set<String> getToUnitNames();

	/**
	 * Sets the available dimensions for filtering.
	 *
	 * @param dimensionNames names of dimensions to use
	 * @since v0.4.0
	 * @since 2021-12-15
	 */
	void setDimensionNames(Set<String> dimensionNames);

	/**
	 * Sets the available units to convert from. {@link #getFromSelection} is not
	 * required to use one of these units; this method is to be used for views
	 * that allow the user to select units from a list.
	 *
	 * @param unitNames names of units to convert from
	 * @since v0.4.0
	 * @since 2021-12-15
	 */
	void setFromUnitNames(Set<String> unitNames);

	/**
	 * Sets the available units to convert to. {@link #getToSelection} is not
	 * required to use one of these units; this method is to be used for views
	 * that allow the user to select units from a list.
	 *
	 * @param unitNames names of units to convert to
	 * @since v0.4.0
	 * @since 2021-12-15
	 */
	void setToUnitNames(Set<String> unitNames);

	/**
	 * Shows the output of a unit conversion.
	 * 
	 * @param input  input unit & value (obtained from this view)
	 * @param output output unit & value
	 * @since v0.4.0
	 * @since 2021-12-24
	 */
	void showUnitConversionOutput(UnitConversionRecord uc);
}
