/**
 * Copyright (C) 2021-2022 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnitsGUI;

import java.util.Optional;
import java.util.Set;

import sevenUnits.unit.UnitType;
import sevenUnits.utils.NameSymbol;

/**
 * An object that controls user interaction with 7Units
 * 
 * @author Adrien Hopkins
 * @since v0.4.0
 * @since 2021-12-15
 */
public interface View {
	/**
	 * @return a new tabbed view
	 * @since v0.4.0
	 * @since 2022-04-19
	 */
	static View createTabbedView() {
		return new TabbedView();
	}

	/**
	 * @return the presenter associated with this view
	 * @since v0.4.0
	 * @since 2022-04-19
	 */
	Presenter getPresenter();

	/**
	 * @return name of prefix currently being viewed
	 * @since v0.4.0
	 * @since 2022-04-10
	 */
	Optional<String> getViewedPrefixName();

	/**
	 * @return name of unit currently being viewed
	 * @since v0.4.0
	 * @since 2022-04-10
	 */
	Optional<String> getViewedUnitName();

	/**
	 * Sets the list of prefixes that are available to be viewed in a prefix
	 * viewer
	 *
	 * @param prefixNames prefix names to view
	 * @since v0.4.0
	 * @since 2022-04-10
	 */
	void setViewablePrefixNames(Set<String> prefixNames);

	/**
	 * Sets the list of units that are available to be viewed in a unit viewer
	 *
	 * @param unitNames unit names to view
	 * @since v0.4.0
	 * @since 2022-04-10
	 */
	void setViewableUnitNames(Set<String> unitNames);

	/**
	 * Shows an error message.
	 *
	 * @param title   title of error message; on any view that uses an error
	 *                dialog, this should be the title of the error dialog.
	 * @param message error message
	 * @since v0.4.0
	 * @since 2021-12-15
	 */
	void showErrorMessage(String title, String message);

	/**
	 * Shows information about a prefix to the user.
	 *
	 * @param name             name(s) and symbol of prefix
	 * @param multiplierString string representation of prefix multiplier
	 * @since v0.4.0
	 * @since 2022-04-10
	 */
	void showPrefix(NameSymbol name, String multiplierString);

	/**
	 * Shows information about a unit to the user.
	 *
	 * @param name          name(s) and symbol of unit
	 * @param definition    unit's definition string
	 * @param dimensionName name of unit's dimension
	 * @param type          type of unit (metric/semi-metric/non-metric)
	 * @since v0.4.0
	 * @since 2022-04-10
	 */
	void showUnit(NameSymbol name, String definition, String dimensionName,
			UnitType type);
}
