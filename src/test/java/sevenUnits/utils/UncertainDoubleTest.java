/**
 * Copyright (C) 2021 Adrien Hopkins
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package sevenUnits.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static sevenUnits.utils.UncertainDouble.fromRoundedString;
import static sevenUnits.utils.UncertainDouble.fromString;
import static sevenUnits.utils.UncertainDouble.of;

import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Tests for the UncertainDouble
 *
 * @author Adrien Hopkins
 * @since 2021-11-29
 */
class UncertainDoubleTest {
	/**
	 * Ensures that the compareTo function behaves correctly.
	 */
	@Test
	final void testCompareTo() {
		assertTrue(of(2.0, 0.5).compareTo(of(2.0, 0.1)) == 0);
		assertTrue(of(2.0, 0.5).compareTo(of(1.0, 0.1)) > 0);
		assertTrue(of(2.0, 0.5).compareTo(of(3.0, 0.1)) < 0);
	}

	/**
	 * Tests the ___exact operations
	 */
	@Test
	final void testExactOperations() {
		final UncertainDouble x = UncertainDouble.of(Math.PI, 0.1);

		// slightly different because roundoff errors
		final UncertainDouble x1 = UncertainDouble.of(Math.PI + Math.E - Math.E,
				0.1);
		final UncertainDouble x2 = UncertainDouble.of(Math.PI * Math.E / Math.E,
				0.1);

		// get results
		final UncertainDouble result1 = x.plusExact(Math.E).minusExact(Math.E);
		final UncertainDouble result2 = x.timesExact(Math.E)
				.dividedByExact(Math.E);

		// test that these operations work & don't change uncertainty
		assertEquals(x1, result1);
		assertTrue(x.equivalent(result1));
		assertEquals(x2, result2);
		assertTrue(x.equivalent(result2));

		// exponents are different
		assertEquals(Math.pow(Math.PI, Math.E),
				x.toExponentExact(Math.E).value());
	}

	/**
	 * Test for {@link UncertainDouble#fromRoundedString}
	 * 
	 * @since 2022-04-18
	 */
	@Test
	final void testFromRoundedString() {
		assertEquals(of(12345.678, 0.001), fromRoundedString("12345.678"));
	}

	/**
	 * Test for {@link UncertainDouble#fromString}
	 */
	@Test
	final void testFromString() {
		// valid strings
		assertEquals(of(2.0, 0.5), fromString("2.0 � 0.5"));
		assertEquals(of(2.0, 0.5), fromString("2.0 +- 0.5"));
		assertEquals(of(2.0, 0.0), fromString("2.0"));

		// invalid strings
		for (final String s : List.of("2.A", "A", "2.0 � .", "� 3.5")) {
			assertThrows(IllegalArgumentException.class, () -> fromString(s));
		}

		// back and forth
		assertEquals("2.0 � 0.5", of(2.0, 0.5).toString());
		assertEquals("2.0", of(2.0, 0).toString());
	}

	@Test
	final void testHashCode() {
		assertEquals(of(2.0, 0.5).hashCode(), fromString("2.0 � 0.5").hashCode());
	}
}
